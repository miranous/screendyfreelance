import { UserEP } from '../config/endpoints';
import ApiHelper from '../utils/api';
import AppDispatcher from '../dispatcher/AppDispatcher';
import UserConstants from '../constants/UserConstants';




class DevelopperActions {
  GetAllJobs(){
    ApiHelper.get( UserEP.GET_ALL_JOBS() )
    .then(data => {
      data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.GET_ALL_JOBS,
        data : data
      });
    });
  }
  Dislike(idApp, idUser) {
   
    var form_data = new FormData();
      form_data.append('idApp', idApp);
      form_data.append('idUser', idUser);
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/Dislike',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){

                                  ApiHelper.get( UserEP.GET_APP_DETAILS(idApp) )
                                  .then(data => {

                                    data.FINE && AppDispatcher.dispatch({
                                      actionType: UserConstants.GET_APP_DETAILS,
                                      data: data
                                    });
                                  });
                                  
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  
  }

  Like(idApp, idOwner, idUser) {

     var form_data = new FormData();
      form_data.append('idApp', idApp);
      form_data.append('idOwner', idOwner);
      form_data.append('idUser', idUser);
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/Like',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){

                                  ApiHelper.get( UserEP.GET_APP_DETAILS(idApp) )
                                  .then(data => {

                                    data.FINE && AppDispatcher.dispatch({
                                      actionType: UserConstants.GET_APP_DETAILS,
                                      data: data
                                    });
                                  });
                                  
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  
  
  }

  GetAppDetails(id) {
           
    ApiHelper.get( UserEP.GET_APP_DETAILS(id) )
    .then(data => {

      data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.GET_APP_DETAILS,
        data : data
      });
    });
  
  }

  GetDeveloppers(rank,type) {
           
    ApiHelper.get( UserEP.GET_DEVS(rank,type) )
    .then(data => {

      data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.GET_DEVS,
        userObject: data
      });
    });
  
  }

  GetDeveloppersTop3() {
           
    ApiHelper.get( UserEP.GET_DEVS_TOP3() )
    .then(data => {

      data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.GET_DEVS,
        userObject: data
      });
    });
  
  }

  GetDeveloppersSearch(search) {
           
    ApiHelper.get( UserEP.GET_DEVS_SEARCH(search) )
    .then(data => {

      data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.GET_DEVS,
        userObject: data
      });
    });
  
  }

  GetDevelopperDetails(id) {
           
    ApiHelper.get( UserEP.GET_DEV_DETAILS(id) )
    .then(data => {

      data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.GET_DEV_DETAILS,
        userObject: data
      });
    });
  
  }

  GetDevelopperApps(id,screendy) {
           
    ApiHelper.get( UserEP.GET_DEV_APPS(id,screendy) )
    .then(data => {

      data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.GET_DEV_APPS,
        userObject: data
      });
    });
  
  }

  AddApp(formdata,id,screendy) {
           
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/addApplication',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: formdata,                   
                              type: 'POST',
                              success: function(response){

                                ApiHelper.get( UserEP.GET_DEV_APPS(id,screendy) )
                                  .then(data => {

                                    data.FINE && AppDispatcher.dispatch({
                                      actionType: UserConstants.GET_DEV_APPS,
                                      userObject: data
                                    });
                                  });

                                  AppDispatcher.dispatch({
                                    actionType: UserConstants.ADD_DEV_APP,
                                    userObject: 1
                                  });
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  
  }

  UpdateProfile(formdata,id) {
           
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/updtFreelance',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: formdata,                   
                              type: 'POST',
                              success: function(response){

                                ApiHelper.get( UserEP.GET_DEV_DETAILS(id) )
                                .then(data => {

                                  data.FINE && AppDispatcher.dispatch({
                                    actionType: UserConstants.GET_DEV_DETAILS,
                                    userObject: data
                                  });
                                });
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  
  }

  UpdateApp(formdata,id,screendy) {
           
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/updApp',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: formdata,                   
                              type: 'POST',
                              success: function(response){

                                ApiHelper.get( UserEP.GET_DEV_APPS(id,screendy) )
                                  .then(data => {

                                    data.FINE && AppDispatcher.dispatch({
                                      actionType: UserConstants.GET_DEV_APPS,
                                      userObject: data
                                    });
                                  });

                                  AppDispatcher.dispatch({
                                    actionType: UserConstants.ADD_DEV_APP,
                                    userObject: 1
                                  });
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  
  }

  RemoveApp(formdata,id,screendy) {
           
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/deleteApp',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: formdata,                   
                              type: 'POST',
                              success: function(response){

                                ApiHelper.get( UserEP.GET_DEV_APPS(id,screendy) )
                                  .then(data => {

                                    data.FINE && AppDispatcher.dispatch({
                                      actionType: UserConstants.GET_DEV_APPS,
                                      userObject: data
                                    });
                                  });

                                  AppDispatcher.dispatch({
                                    actionType: UserConstants.ADD_DEV_APP,
                                    userObject: 1
                                  });
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  
  }

  getComponentsByDev(id){
     ApiHelper.get( UserEP.DEV_DATA(id) )
    .then(data => {

     data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.DEV_DATA,
        data: data
      });
    
    });

  }

  getTemplates(id,freelance){
     ApiHelper.get( UserEP.GET_TEMPLATES(id,freelance) )
    .then(data => {

     data.FINE && AppDispatcher.dispatch({
        actionType: UserConstants.DEV_TEMPLATES,
        data: data
      });
    
    });

  }

  Follow(idfollowed, idfollower)
  {
    var form_data = new FormData();
      form_data.append('user', idfollowed);
      form_data.append('follower', idfollower);
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/followme',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){

                                  ApiHelper.get( UserEP.GET_DEV_DETAILS(idfollowed) )
                                  .then(data => {

                                    data.FINE && AppDispatcher.dispatch({
                                      actionType: UserConstants.GET_DEV_DETAILS,
                                      userObject: data
                                    });
                                  });
                                  
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  }

  UnFollow(idfollowed, idfollower)
  {
    var form_data = new FormData();
      form_data.append('user', idfollowed);
      form_data.append('follower', idfollower);
    $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/unfollowme',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){

                                  ApiHelper.get( UserEP.GET_DEV_DETAILS(idfollowed) )
                                  .then(data => {

                                    data.FINE && AppDispatcher.dispatch({
                                      actionType: UserConstants.GET_DEV_DETAILS,
                                      userObject: data
                                    });
                                  });
                                  
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                              
                                            }
                                 });
  }

}

export default new DevelopperActions;