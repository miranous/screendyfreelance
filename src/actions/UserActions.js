import { UserEP } from '../config/endpoints';

import ApiHelper from '../utils/api';

import AppDispatcher from '../dispatcher/AppDispatcher';
import UserConstants from '../constants/UserConstants';


/*
 * UserActions
 */

class UserActions {

  getCompanyProfil(idcompany){
    ApiHelper.get( UserEP.COMPANY_PROFIL(idcompany) )
    .then(data => {

      AppDispatcher.dispatch({
        actionType: UserConstants.COMPANY_PROFIL,
        data: data
      });
    
    });
  }

  signUpTheUser(user) {
      
    // Asynch stuff and localstorage, then dispatch        
    ApiHelper.get( UserEP.SIGN_UP( user ) )
    .then(data => {

      if(data.FINE) {

        AppDispatcher.dispatch({
          actionType: UserConstants.SIGN_UP,
          message: data
        });
      }

      if(data.NOT_FINE) {

        AppDispatcher.dispatch({
          actionType: UserConstants.SIGN_UP_FAIL,
          message: data
        });

      }
      
    
    });
  
  }

  signTheUserIn(user) {
      
    // Asynch stuff and localstorage, then dispatch        
    ApiHelper.get( UserEP.SIGN_IN( user ) )
    .then(data => {

      if(data.FINE) {

        AppDispatcher.dispatch({
          actionType: UserConstants.SIGN_IN,
          userObject: data
        });
      }

      if(data.NOT_FINE) {

        AppDispatcher.dispatch({
          actionType: UserConstants.SIGN_IN_FAIL,
          message: data.message
        });

      }
      
    
    });
  
  }

  is_active() {
      
    // Asynch stuff and localstorage, then dispatch        
    ApiHelper.get( UserEP.IS_ACTIVE() )
    .then(data => {

      AppDispatcher.dispatch({
        actionType: UserConstants.IS_ACTIVE,
        userObject: data
      });
    
    });
  
  }

  is_freelancer(id) {
      
    // Asynch stuff and localstorage, then dispatch        
    ApiHelper.get( UserEP.IS_FREELANCER(id) )
    .then(data => {

      AppDispatcher.dispatch({
        actionType: UserConstants.IS_FREELANCER,
        userObject: data
      });
    
    });
  
  }

  is_company(id) {
      
    // Asynch stuff and localstorage, then dispatch        
    ApiHelper.get( UserEP.IS_COMPANY(id) )
    .then(data => {

      AppDispatcher.dispatch({
        actionType: UserConstants.IS_COMPANY,
        userObject: data
      });
    
    });
  
  }

  is_active_popup() {
      
    // Asynch stuff and localstorage, then dispatch        
    ApiHelper.get( UserEP.IS_ACTIVE() )
    .then(data => {

      AppDispatcher.dispatch({
        actionType: UserConstants.IS_ACTIVE_POPUP,
        userObject: data
      });
    
    });
  
  }

  sign_out() {
      
    // Asynch stuff and localstorage, then dispatch        
    ApiHelper.get( UserEP.SIGN_OUT() )
    .then(data => {

      AppDispatcher.dispatch({
        actionType: UserConstants.SIGN_OUT,
        userObject: data
      });
    
    });
  
  }

}

export default new UserActions;
