import React, { Component } from 'react';
import JobComponent from './job';
//GetAllJobs
import DevelopperActions from '../actions/DevelopperActions';
import JobStore from '../stores/JobStore';

export default class JobList extends Component {

  constructor(props, context) {
    super(props, context);
    this.state={
     "jobs": {}
    }
    DevelopperActions.GetAllJobs();
    this.onChange=this.onChange.bind(this);
    this.createJob=this.createJob.bind(this);
  }

  createJob(job){
    return(<JobComponent  job={job} key={job.jobid}/>);
  }
  onChange(){
      this.setState({
       jobs : JobStore.getJobs()
      });
  }
  componentWillMount(){
    JobStore.addChangeListener(this.onChange);
  }
  componentWillUnmount(){
    JobStore.removeChangeListener(this.onChange);
  }
  render() {
    if(this.state.jobs.length > 0)
    {
    return (
     <div className ="row">
     {this.state.jobs.map(this.createJob, this)}
    </div>
    );
    }
    else
    return <div/>;
  }

}