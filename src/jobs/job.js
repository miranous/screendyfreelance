import React, { Component } from 'react';
import { Link } from 'react-router';

export default class Job extends Component {

  constructor(props, context) {
    super(props, context);
    this.state={
      job : this.props.job
    }
    this.onClick=this.onClick.bind(this);
  }

  onClick(){
    
  }

  render() {
    return (
       <div>
        <div className="col-md-4">
          <div className="box box-widget widget-user-2">
            <div className="widget-user-header bg-blue">
              <div className="widget-user-image">
                <img className="img-circle" src={this.state.job.company.logocompany} alt="User Avatar"/>
              </div>
              <h5 className="widget-user-desc"><b>{this.state.job.company.namecompany}</b> is looking for </h5>
              <h5 className="widget-user-desc">{this.state.job.jobtitle}</h5>
            </div>
            <div className="box-footer no-padding">
              <ul className="nav nav-stacked">
                <li><a><i className="fa fa-map-marker"/> Company location <span className="pull-right badge bg-blue">{this.state.job.company.locationcompany}</span></a></li>
                <li><a><i className="fa fa-calendar-times-o"/> Post date <span className="pull-right badge bg-aqua">{this.state.job.datejob.split(" ")[0]}</span></a></li>
                <li className="btn btn-default text-center"><Link to={`jobs/JobDetails/${this.state.job.jobid}`}><b>See more</b></Link></li>
              </ul>
            </div>
          </div>
        </div>

        
      </div>
    );
  }

}