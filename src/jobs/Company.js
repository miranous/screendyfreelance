import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';
import UserStore from '../stores/UserStore';
import DevelopperActions from '../actions/DevelopperActions';
import userActions from '../actions/UserActions';
import CompanyStore from '../stores/CompanyStore';
import { Modal } from 'react-bootstrap';

export default class ProfileCompany extends React.Component{
	constructor(props, context) {
    super(props, context);
    if(!UserStore.isSignedIn())
    {
    	this.props.history.pushState(null, `signin`);
    }
    this.state={
      job : null,
      showresult : false,
      addjobresponse : false,
      errors : {},
      addjob: false
    }
    userActions.usergetCompanyProfil(UserStore.getUser().id);
    this.onChange=this.onChange.bind(this);
    this.Update = this.Update.bind(this);
    this.AddJob= this.AddJob.bind(this);
    this.showresultClose=this.showresultClose.bind(this);
    this.ComponentFormIsValid = this.ComponentFormIsValid.bind(this);
    this.addjobClose=this.addjobClose.bind(this);
    this.handleaddjob=this.handleaddjob.bind(this);
    this.addjobresponseClose=this.addjobresponseClose.bind(this);
  	}
addjobresponseClose(){
	this.setState({
    	addjobresponse : false
    });
    this.props.history.pushState(null, `/`);
}
 addjobClose(){
  	this.setState({
    	addjob : false
    });
  	}
 handleaddjob(){
    this.setState({
      addjob: true
    });
  }
  showresultClose(){
    this.setState({
    	showresult : false
    });
  }

  componentWillMount(){
    CompanyStore.addChangeListener(this.onChange);
  }
  
  componentWillUnmount(){
    CompanyStore.removeChangeListener(this.onChange);
  }

  onChange(){
    
    var job = CompanyStore.getCompany();
 
        this.setState({
          job : job
        });
  
  }

  ComponentFormIsValid() {
        var formIsValid = true;
        this.state.errors = {};

      if (! this.refs.title.value || this.refs.title.length < 1) {
        this.state.errors.title = 'Please fill in the title of your post';
        formIsValid = false;
      }

     if (! this.refs.skills.value || this.refs.skills.length < 1) {
        this.state.errors.skills = 'Please fill in the skills you want';
        formIsValid = false;
      }

      if (! this.refs.price.value || this.refs.price.length < 1) {
        this.state.errors.price = 'Please fill in your price.';
        formIsValid = false;
      }
      if (! this.refs.description.value || this.refs.description.length < 1) {
        this.state.errors.description = 'Please fill in your description';
        formIsValid = false;
      }
      console.log('ERRORS:::: ',this.state.errors);
        this.setState({errors: this.state.errors});
        return formIsValid;

  }


  AddJob() {
  	  if (!this.ComponentFormIsValid()) {
        event.preventDefault();
        return;
      }
      else{
      var form_data = new FormData();
      var that=this;
      form_data.append('companyid', UserStore.getUser().id);
      form_data.append('jobtitle', this.refs.title.value);
      form_data.append('jobskills', this.refs.skills.value);
      form_data.append('jobprice', this.refs.price.value);
      form_data.append('jobdescription', this.refs.description.value);
      $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/addJob',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){
                             	that.setState({
							    	addjobresponse : true
							    });
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                           

                              }
                   });
  	}
   }

  Update() {
      event.preventDefault();
      var form_data = new FormData();
      var that = this;
      form_data.append('idUser', UserStore.getUser().id);
      form_data.append('namecompany', this.refs.companyname.value);
      form_data.append('avatar', this.refs.companylogo.files[0]);
      form_data.append('avatarbackup', this.state.job.company.logocompany);
      form_data.append('emailcompany', this.refs.companyemail.value);
      form_data.append('locationcompany', this.refs.companylocation.value);
      form_data.append('descriptioncompany', this.refs.overview.value);
      $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/updateCompany',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){
                                that.setState({ 
						            showresult : true
						             });
                                userActions.usergetCompanyProfil(UserStore.getUser().id);

                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                
                              }
                   });
        userActions.usergetCompanyProfil(UserStore.getUser().id);
   }

  render() {
  	if(this.state.job!=null)
    {
    return (
      <div className="container">
      <div className="col-md-4">
            <div className="box box-primary ">
              <div className="box-body box-profile bg-blue-active">
          
                <div className="text-center">
                  <img className="profile-user-img img-circle centerimage" src={this.state.job.company.logocompany} alt="User Avatar" />
                </div>
                <br /><br />
                <label>Update your personal informations</label>
                <div className="form-group">
                <label>Company logo</label>
                
                      <input type="file"
                        name="avatar"
                        className="form-control"
                        placeholder="avatar"
                        ref="companylogo"/>

                </div>
                <div className="form-group">
                <label>Company name</label>
                
                      <input type="text"
                          name="namecompany"
                          defaultValue={this.state.job.company.namecompany}
                          className="form-control"
                          placeholder="company name"
                          ref="companyname"/>

                    
                </div>
                <div className="form-group">
                <label>Company email</label>
                 
                      <input type="text"
                          name="email"
                          defaultValue={this.state.job.company.emailcompany}
                          className="form-control"
                          placeholder="Company email"
                          ref="companyemail"/>

                </div>
               <div className="form-group">
                <label>Company location</label>

                      <input type="text"
                          name="location"
                          defaultValue={this.state.job.company.locationcompany}
                          className="form-control"
                          placeholder="Company location"
                          ref="companylocation"/>

                </div>
            </div>     
           </div> 
           </div> 


           <div className="col-md-8">
          <div className="nav-tabs-custom">
            <ul className="nav nav-tabs">
              <li className="active"><a>Overview</a></li>
              <li className="pull-right"><button onClick={this.Update} type="button" className="btn btn-success">Save All Changes</button></li>
              <li className="pull-right"><button type="button" className="btn btn-default" onClick={this.handleaddjob}>Post a job</button></li>
              
            </ul>
            <div className="tab-content">
              <div className="form-group">
                <b>Write a description about your company and what it does...</b>
                <br/><br/>
                  <div className="">
                    <textarea
                        name="overview"
                        className="form-control"
                        defaultValue={this.state.job.company.descriptioncompany}
                        placeholder="Overview.."
                        style={{resize:"none"}}
                        rows="6"
                        id="overview"
                        ref="overview"/>
                  </div>
              </div>
            </div>
          </div>
          
         </div>
           	<Modal show={this.state.showresult} onHide={this.showresultClose}>
                <Modal.Body>
                 <i className="fa fa-check "></i> Your profile was successfully updated 
                </Modal.Body>
          	</Modal>
          	<Modal show={this.state.addjobresponse}>
                <Modal.Body>
                 <i className="fa fa-check "></i> The job was successfully added
                </Modal.Body>
                <Modal.Footer>
                <button type="button" className="btn btn-success" onClick={this.addjobresponseClose}>Great !</button>
                </Modal.Footer>
          	</Modal>
          <Modal show={this.state.addjob} onHide={this.addjobClose}>
            <div className="modal-dialog">

              <div className="modal-content ">
                <div className="modal-header">
                  <button type="button" className="close" onClick={this.addjobClose}>&times;</button>
                  <h4 className="modal-title">Post your job using ScreenDy</h4>
                </div>
                <div className="modal-body">
                <div>

                <div className="form-group">
                  <label >Job title</label>
                  <input type="text" className="form-control" ref="title"/>
                  <div className="input text-red">{this.state.errors.title}</div>
                </div>


                <div className="form-group">
                  <label >Job category</label>
                  <select className="form-control" id="sel1">
                    <option>ScreenDy component</option>
                    <option>Application</option>
                  </select>
                </div>

                <div className="form-group">
                  <label >Skills required</label>
                  <input type="text" className="form-control" ref="skills"/>
                   <div className="input text-red">{this.state.errors.skills}</div>
                </div>
                <div className="form-group">
                  <label >Price </label>
                  <input type="text" className="form-control" ref="price"/>
                   <div className="input text-red">{this.state.errors.price}</div>
                </div>
                <div className="form-group">
                  <label >Job description</label>
                  <textarea type="textarea" className="form-control" ref="description"/>
                   <div className="input text-red">{this.state.errors.description}</div>
                </div>
                </div>
                </div>

                <div className="modal-footer">
                	<button type="button" className="btn btn-success" onClick={this.AddJob}>Add</button>
                    <button type="button" className="btn btn-default" onClick={this.addjobClose}>Close</button>
                </div>

              </div>

            </div>
          </Modal>
          
       </div>      
    );
	}
	else{
		return(<div/>);
	}
  }
}