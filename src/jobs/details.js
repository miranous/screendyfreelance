import React, { Component } from 'react';
import { Link } from 'react-router';
import DevelopperActions from '../actions/DevelopperActions';
import JobStore from '../stores/JobStore';

export default class JobDetails extends Component {

  constructor(props, context) {
    super(props, context);
    this.state={
     jobs : null
    }
    DevelopperActions.GetAllJobs();
    this.onChange=this.onChange.bind(this);
    this.createSkill = this.createSkill.bind(this);
  }
   onChange(){
    for(var i=0;i<JobStore.getJobs().length;i++)
    {
      var job = JobStore.getJobs()[i];
      if(job.jobid==this.props.params.id)
      {
        this.setState({
          jobs : job
        });
        
      }
    }
      
  }
  componentWillMount(){
    JobStore.addChangeListener(this.onChange);
  }
  componentWillUnmount(){
    JobStore.removeChangeListener(this.onChange);
  }

  createSkill(skill){
  	return(<span className="label label-primary" key={skill}>&nbsp;{skill}&nbsp;</span>);
  }

  render() {
    if(this.state.jobs)
    {
    //var skills = JSON.parse(this.state.jobs.jobskills);
    return (
      <div className="container">
       <section className="content-header">
              <h4>
                Details
              </h4>
              <ol className="breadcrumb">
                <li><Link to='jobs'><i className="fa fa-dashboard"></i>Jobs</Link></li>
                <li className="active">Details</li>
              </ol>
        </section>
       <div className="col-md-9">
       <div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Job offer</h3>
            </div>
          
            <div className="box-body">
            	<div className="container col-md-12">	
	              <img className="profile-user-img img-responsive img-circle col-md-2" src={this.state.jobs.company.logocompany} alt="User profile picture"/>
	              
	              <div className="col-md-9">
		              <h3><strong >{this.state.jobs.company.namecompany}</strong> </h3>
		              <p>is Looking for a <strong>{this.state.jobs.jobtitle}</strong></p>
		           </div>
		           <div className="col-md-1">
		              <h4><strong>{this.state.jobs.jobprice}</strong></h4>
		           </div>
	             </div>

              
              <br/><br/><br/><br/><br/>
              <hr />
              <strong><i className="fa fa-file-text-o margin-r-5"></i>Overview</strong>

              <p className="text-muted">{this.state.jobs.jobdescription}</p>

              <hr/>

              <strong><i className="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
              
              </p>

            
            </div>
          
          </div>
       </div>


       <div className="col-md-3">
       <button type="button"  className="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">Apply to the job</button>
      	<br/>
      	<div className="box box-primary">
            <div className="box-header with-border">
              <h3 className="box-title">Informations</h3>
            </div>
          
            <div className="box-body">

              <p>
                If you are a ScreenDy freelancer, you can apply to a job offer by clicking the button above. 
                In case you aren't, you can Register to get the best job offers and have the opportunity to apply to the ones you prefer.
              </p>

            </div>
          
          </div>
       </div>

       <div id="myModal" className="modal fade" role="dialog">
              <div className="modal-dialog">
                   <div className="modal-content">
                    <div className="box-header">
                      <i className="fa fa-envelope"></i>

                      <h3 className="box-title">Post for a job</h3>
                      <div className="pull-right box-tools">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                          
                      </div>
                    </div>
                    <div className="box-body">
                      <form action="#" method="post">
                        <div className="form-group">
                                <label>Your email</label>
                                <div>
                                    <div className="form-group">
                                        <input type="text"
                                          name="email"
                                          className="form-control"
                                          placeholder="Email"
                                          ref="email"/>
                                      </div>
                                </div>
                              </div>
                        <div className="form-group">
                                <label>Object</label>
                                <div>
                                    <div className="form-group">
                                        <input type="text"
                                          name="subject"
                                          className="form-control"
                                          placeholder="Subject"
                                          ref="subject"/>
                                      </div>
                                </div>
                              </div>

                              <div className="form-group">
                                <label>Message</label>
                                <div>
                                    <div className="form-group">
                                        <textarea
                                          name="message"
                                          className="form-control"
                                          placeholder="Message"
                                          style={{resize:"none"}}
                                          rows="12"
                                          ref="message"/>
                                      </div>
                                </div>
                              </div>
                      </form>
                    </div>

                    <div className="modal-footer">
                    <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" className="btn btn-primary pull-right" >Send</button>
                  </div>
                  </div>
                  </div>
              </div>
        </div> 
    );
  }
  else
    return (<div/>);
  }

}