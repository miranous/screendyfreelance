import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute } from 'react-router';
// import routes from './config/routes'
import { createHistory, useBasename } from 'history'

import Container from './containers/Container';
import Signin from './containers/Signin';
import Signup from './containers/Signup';
import Home from './containers/Home';
import Become from './containers/Become';
import Hire from './containers/Hire';
import Developpers from './containers/Developpers';
import DevDetails from './developpers/Details';
import Profile from './developpers/Profile';
import ProfileCompany from './jobs/Company';
import Jobs from './containers/Jobs';
import Choose from './containers/Choose';
import Search from './containers/Search' ;
import AppDetails from './developpers/AppDetails';
import ReactCropper from './developpers/ReactCropper';
import JobDetails from './jobs/details';

const history = useBasename(createHistory)({
  basename: '/'
})

// <Route name='signin' path='/signin' component={UserPage} />

render(
	<Router>
        <Route name='index' path='/' component={Container}>
        	<IndexRoute component={Home}/>
            <Route name='Home' path='Home' component={Home}/>
        	<Route name='signin' path='signin' component={Signin} />
            <Route name='signup' path='signup' component={Signup} />
            <Route name='becomedev' path='becomedev' component={Become} />
            <Route name='hiredev' path='hiredev' component={Hire} />
        	<Route name='developpers' path='developpers/:page/:rank' component={Developpers}/>
        	<Route name='jobs' path='jobs' component={Jobs}/>
            <Route name='Search' path='Search/:page/:keyword' component={Search}/>
            <Route name='dev_details' path='Details/:id' component={DevDetails}/>
            <Route name='profile' path='profile/:id' component={Profile}/>
            <Route name='profilecompany' path='profilecompany/:id' component={ProfileCompany}/>
            <Route name='AppDetails' path='AppDetails/:id' component={AppDetails}/>
            <Route name='Choose' path='Choose' component={Choose}/>
            <Route name='ReactCropper' path='ReactCropper' component={ReactCropper}/>
            <Route name='JobDetails' path='jobs/JobDetails/:id' component={JobDetails} />
        </Route>
    </Router>
    ,
    document.getElementById('sd-main-app-container')
);
