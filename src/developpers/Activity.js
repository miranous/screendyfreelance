import React from 'react';
import { render } from 'react-dom';
import { Link } from 'react-router';

export default class Activity extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.state={
      activities : this.props.activities
    }
    this.createActivity=this.createActivity.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      activities : nextProps.activities
    });
  }
  createActivity(activity){
    let avatar=null;
  if(activity.ActivityUser)
   avatar =activity.ActivityUser.avatar;
  console.log(activity.ActivityUser.avatar);
  if(activity.type==="Like")
    {
    return( <li key={activity.id}>
              <i className="fa fa-heart bg-blue"></i>

              <div className="timeline-item">
                <span className="time"><i className="fa fa-clock-o"></i> {activity.date}</span>
                <img className="direct-chat-img img-timeline" src={avatar} />
                <h3 className="timeline-header"><Link to={`Details/${activity.ActivityUser.id}`}> {activity.ActivityUser.firstname} {activity.ActivityUser.lastname}</Link> Liked <Link to={`AppDetails/${activity.ActivityApp.id}`}>{activity.ActivityApp.appName}</Link></h3>

                <div className="timeline-body">
                  <img className="img-rounded" src={activity.ActivityApp.appPreview} width="100"/>
                </div>
              </div>
            </li>);
  }
  else
  {
   return(<li  key={activity.id}>
              <i className="fa fa-user bg-aqua"></i>

              <div className="timeline-item">
                <span className="time"><i className="fa fa-clock-o"></i> {activity.date}</span>
                <img className="direct-chat-img img-timeline" src={activity.ActivityUser.avatar} />
                <h3 className="timeline-header no-border"><Link to={`Details/${activity.ActivityUser.id}`}>{activity.ActivityUser.firstname} {activity.ActivityUser.lastname}</Link> Started following you</h3>
              </div>
            </li>);
  }
  }

  render() {
    if(this.state.activities[0])
    {
    return (
    <div>
    <ul className="timeline timeline-inverse">
            <li className="time-label">
                  <span className="bg-gray">
                  {this.state.activities[0].date}
                  </span>
            </li>    
    {this.state.activities.map(this.createActivity, this)}
            <li>
              <i className="fa fa-clock-o bg-gray"></i>
            </li>
    </ul>
    </div>
    );
  }else
  return <div/>;
  }
}