import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';
import { Link } from 'react-router';

export default class App extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.state={
     project : this.props.project
    }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
     project : nextProps.project
    });
  }
  render() {
    return (
     <div className="col-md-4 text-center">
     <Link to={`AppDetails/${this.state.project.id}`}>
     <img id={this.state.project.id} src={this.state.project.preview1} alt="Pulpit Rock" height="300" width="180" className="logomargin"/>
     <br />
     </Link>
     <b >{this.state.project.name}</b>
     <br /><br />
     </div>
    );
  }
}