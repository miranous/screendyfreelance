import React, { Component } from 'react';
import { Link } from 'react-router';
import DevelopperActions from '../actions/DevelopperActions';
export default class PaginationSearch extends Component {

  constructor(props, context) {
    super(props, context);
    this.state={
    	i : this.props.i,
    	search : this.props.search
    }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      i : nextProps.i,
      search : nextProps.search
    });
  }
   
  render() {
    return (
     <li><Link to={`Search/${this.state.i}/${this.state.search}`}>{this.state.i}</Link></li>
    );
  }

}