import React, { Component } from 'react';
import { Link } from 'react-router';
import DevelopperActions from '../actions/DevelopperActions';
import ComponentStore from '../stores/ComponentStore';

export default class ComponentDev extends Component {

  constructor(props, context) {
    super(props, context);
    DevelopperActions.getTemplates(this.props.id,1);
    this.state={
      templates : null
    }
    this.onChange=this.onChange.bind(this);
    this.createTemplate=this.createTemplate.bind(this);
  }
  onChange(){
      this.setState({
        templates : ComponentStore.getDevTemplates()
      });
  }
  componentWillMount(){
    ComponentStore.addChangeListener(this.onChange);
  }
  componentWillUnmount(){
    ComponentStore.removeChangeListener(this.onChange);
  }
  createTemplate(template){
    var r = <div className="col-md-4 text-center" key={template.id}>
                  <img src={template.previews[0].urlPreview} height="300" width="180" className="img-rounded "/>
                <p><b>{template.componentName}</b></p>
      </div>;
      if(template.status === "PUBLISHED"){
        r = <div className="col-md-4 text-center" key={template.id}>
                <a  href={`http://screendymarket.cloudapp.net:8080/MarketplaceSites/market/#/Templates/Details/${template.id}`} target="_blank">
                  <img src={template.previews[0].urlPreview} height="300" width="180" className="img-rounded "/>
                </a>
                <p><b>{template.componentName}</b></p>
      </div>;
      }
    return( r);
  }
  render() {
    if(this.state.templates == null || this.state.templates.length < 1)
    return <div />
    return (
      <div className="row">
      {this.state.templates.map(this.createTemplate, this)}
      </div>
      
    );
  }

}