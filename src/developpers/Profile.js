import React, { Component } from 'react';
import DevList from '../developpers/DeveloppersList';
import ComponentDev from './ComponentDev';
import Applications from './Applications';
import { Link } from 'react-router';
import DevelopperActions from '../actions/DevelopperActions';
import DevStore from '../stores/DevStore';
import FreelancerStore from '../stores/FreelancerStore';
import UserStore from '../stores/UserStore';
import TagsInput from 'react-tagsinput';
import { Modal } from 'react-bootstrap';
import UserActions from '../actions/UserActions';
import Realization from './Realization'; 
import Follower from './Follower';


export default class Profile extends Component {

  constructor(props, context) {
    super(props, context);
    DevelopperActions.GetDevelopperDetails(this.props.params.id);
    this.state={
      developper : null,
      errors: {},
      skills:[],
      showmodal: false,
      loading : false,
      showresult : false
    }
    this.onChange=this.onChange.bind(this);
    this.handleSkillsChange = this.handleSkillsChange.bind(this);
    this.update=this.update.bind(this);
    this.showresultClose=this.showresultClose.bind(this);
  }

  componentDidMount () {
    window.scrollTo(0, 0);
  }

  
  onChange(){
    this.setState({
      developper : DevStore.getDevDetails(),
      skills : JSON.parse(DevStore.getDevDetails().skills),
      showmodal: false
      
    });
    if(UserStore.getUser().id != this.props.params.id) {
          this.props.history.pushState(null, `/`);
        }

    CKEDITOR.replace('overview');

  }

  handleSkillsChange(skills) {
        this.setState({skills})
      }

  componentWillMount(){
    
    DevStore.addChangeListener(this.onChange);
  }
  componentWillUnmount(){
    DevStore.removeChangeListener(this.onChange);
  }

  update() {

    var form_data = new FormData();
      form_data.append('idUser', UserStore.getUser().id);
      form_data.append('firstname', this.refs.firstname.value);
      form_data.append('lastname', this.refs.lastname.value);
      form_data.append('email', this.refs.email.value);
      form_data.append('country', this.refs.country.value);
      form_data.append('city', this.refs.city.value);
      form_data.append('description', this.refs.description.value);
      form_data.append('overview', CKEDITOR.instances.overview.getData());
      form_data.append('skills', JSON.stringify(this.state.skills));
      form_data.append('facebook', this.refs.facebook.value);
      form_data.append('color', this.refs.color.value);
      form_data.append('linkedin', this.refs.linkedin.value);
      form_data.append('git', this.refs.git.value);
      form_data.append('twitter', this.refs.twitter.value);
      form_data.append('avatar', this.refs.avatar.files[0]);
      form_data.append('avatarbackup', this.state.developper.avatar);
      DevelopperActions.UpdateProfile(form_data,this.props.params.id);
      this.setState({ 
                      showresult : true
                    });
      UserActions.is_freelancer(UserStore.getUser().id);
  }

  showresultClose(){
    this.setState({showresult : false});
  }

  render() {

    var wrapperClassFirstname = 'form-group';
          if (this.state.errors.firstname && this.state.errors.firstname.length > 0) {
            wrapperClassFirstname += " " + 'has-error';
          }
        var wrapperClassLastname = 'form-group';
          if (this.state.errors.lastname && this.state.errors.lastname.length > 0) {
            wrapperClassLastname += " " + 'has-error';
          }
        var wrapperClassEmail = 'form-group';
          if (this.state.errors.email && this.state.errors.email.length > 0) {
            wrapperClassEmail += " " + 'has-error';
          }
        var wrapperClassDescription = 'form-group';
          if (this.state.errors.description && this.state.errors.description.length > 0) {
            wrapperClassDescription += " " + 'has-error';
          }
        var wrapperClassOverview = 'form-group';
          if (this.state.errors.overview && this.state.errors.overview.length > 0) {
            wrapperClassOverview += " " + 'has-error';
          }
        var wrapperClassAvatar = 'form-group';
          if (this.state.errors.avatar && this.state.errors.avatar.length > 0) {
            wrapperClassAvatar += " " + 'has-error';
          }


    if(this.state.developper==null)
      return <div className="text-center">
            <b className="text-center">Loading, please wait.. <i className="fa fa-refresh fa-spin"></i></b> 
            <br />
            <br />
          </div>;
    return (
      <div className="container">
        <section className="content-header">
              <br/>
              <br/>
              <ol className="breadcrumb">
                <li><Link to='home'><i className="fa fa-dashboard"></i>Freelancers</Link></li>
                <li className="active">Profile</li>
              </ol>
          </section>
        <div className="row">
          <div className="col-md-4">
            <div className="box box-primary ">
              <div className={`box-body box-profile bg-${this.state.developper.color}-active`}>
              <Link to={`Details/${this.state.developper.idUser}`}>
                <div className="text-center">
                  <img className="profile-user-img img-circle centerimage" src={this.state.developper.avatar} alt="User Avatar" />
                </div>
              </Link>
                <br /><br />
                <label>Update your personal informations</label>
                <div className="form-group">
                <label>Profile picture</label>
                    <div className={wrapperClassAvatar}>
                      <input type="file"
                        name="avatar"
                        className="form-control"
                        placeholder="avatar"
                        ref="avatar"/>
                      <div className="input text-red">{this.state.errors.avatar}</div>
                    </div>
                </div>
                <div className="form-group">
                <label>First name</label>
                  <div>
                    <div className={wrapperClassFirstname}>
                      <input type="text"
                          name="firstname"
                          defaultValue={this.state.developper.firstname}
                          className="form-control"
                          placeholder="First Name"
                          ref="firstname"/>
                      <div className="input text-red">{this.state.errors.firstname}</div>
                    </div>
                  </div>
                </div>
                <div className="form-group">
                <label>Last name</label>
                  <div>
                    <div className={wrapperClassLastname}>
                      <input type="text"
                          name="lastname"
                          defaultValue={this.state.developper.lastname}
                          className="form-control"
                          placeholder="Last Name"
                          ref="lastname"/>
                      <div className="input text-red">{this.state.errors.lastname}</div>
                    </div>
                  </div>
                </div>
                <h5 className="widget-user-desc">
                <div className="form-group">
                <label>Job Title</label>
                  <div>
                    <div className={wrapperClassDescription}>
                      <select
                        className="form-control"
                        id="job" 
                        name="job"
                        defaultValue={this.state.developper.description}
                        ref="job"
                        ref="description">
                          <option value="">Job...</option>
                          <option value="Frontend developer">Frontend developer</option>
                          <option value="Backend developer">Backend developer</option>
                          <option value="Mobile developer">Mobile developer</option>
                          <option value="ScreenDy developer">ScreenDy developer</option>
                          <option value="UI Designer">UI Designer</option>
                      </select>
                      
                      <div className="input text-red">{this.state.errors.description}</div>
                    </div>
                  </div>
                </div>
                </h5>
              </div>
              <div className="box-body box-footer">
                <ul className="nav nav-stacked">
                  <div className="form-group">
                  <label>Email</label>
                    <div>
                      <div className={wrapperClassEmail}>
                        <input type="text"
                            name="email"
                            defaultValue={this.state.developper.email}
                            className="form-control"
                            placeholder="Email"
                            ref="email"/>
                        <div className="input text-red">{this.state.errors.email}</div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div>
                      <div className='form-group'>
                      <label>Country</label>
                        <select
                          className="form-control"
                          defaultValue={this.state.developper.country}
                          id="country" 
                          name="country"
                          ref="country">
                            <option value="">Country...</option>
                            <option value="Afganistan">Afghanistan</option>
                            <option value="Albania">Albania</option>
                            <option value="Algeria">Algeria</option>
                            <option value="American Samoa">American Samoa</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Angola">Angola</option>
                            <option value="Anguilla">Anguilla</option>
                            <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Armenia">Armenia</option>
                            <option value="Aruba">Aruba</option>
                            <option value="Australia">Australia</option>
                            <option value="Austria">Austria</option>
                            <option value="Azerbaijan">Azerbaijan</option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                            <option value="Bermuda">Bermuda</option>
                            <option value="Bhutan">Bhutan</option>
                            <option value="Bolivia">Bolivia</option>
                            <option value="Bonaire">Bonaire</option>
                            <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Brazil">Brazil</option>
                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                            <option value="Brunei">Brunei</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Burundi">Burundi</option>
                            <option value="Cambodia">Cambodia</option>
                            <option value="Cameroon">Cameroon</option>
                            <option value="Canada">Canada</option>
                            <option value="Canary Islands">Canary Islands</option>
                            <option value="Cape Verde">Cape Verde</option>
                            <option value="Cayman Islands">Cayman Islands</option>
                            <option value="Central African Republic">Central African Republic</option>
                            <option value="Chad">Chad</option>
                            <option value="Channel Islands">Channel Islands</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Christmas Island">Christmas Island</option>
                            <option value="Cocos Island">Cocos Island</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Comoros">Comoros</option>
                            <option value="Congo">Congo</option>
                            <option value="Cook Islands">Cook Islands</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Cote DIvoire">Cote D'Ivoire</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Cuba">Cuba</option>
                            <option value="Curaco">Curacao</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Czech Republic">Czech Republic</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Djibouti">Djibouti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="Dominican Republic">Dominican Republic</option>
                            <option value="East Timor">East Timor</option>
                            <option value="Ecuador">Ecuador</option>
                            <option value="Egypt">Egypt</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                            <option value="Eritrea">Eritrea</option>
                            <option value="Estonia">Estonia</option>
                            <option value="Ethiopia">Ethiopia</option>
                            <option value="Falkland Islands">Falkland Islands</option>
                            <option value="Faroe Islands">Faroe Islands</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="French Guiana">French Guiana</option>
                            <option value="French Polynesia">French Polynesia</option>
                            <option value="French Southern Ter">French Southern Ter</option>
                            <option value="Gabon">Gabon</option>
                            <option value="Gambia">Gambia</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Germany">Germany</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Gibraltar">Gibraltar</option>
                            <option value="Great Britain">Great Britain</option>
                            <option value="Greece">Greece</option>
                            <option value="Greenland">Greenland</option>
                            <option value="Grenada">Grenada</option>
                            <option value="Guadeloupe">Guadeloupe</option>
                            <option value="Guam">Guam</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guinea">Guinea</option>
                            <option value="Guyana">Guyana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Hawaii">Hawaii</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hong Kong">Hong Kong</option>
                            <option value="Hungary">Hungary</option>
                            <option value="Iceland">Iceland</option>
                            <option value="India">India</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="Iran">Iran</option>
                            <option value="Iraq">Iraq</option>
                            <option value="Ireland">Ireland</option>
                            <option value="Isle of Man">Isle of Man</option>
                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japan">Japan</option>
                            <option value="Jordan">Jordan</option>
                            <option value="Kazakhstan">Kazakhstan</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Kiribati">Kiribati</option>
                            <option value="Korea North">Korea North</option>
                            <option value="Korea Sout">Korea South</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                            <option value="Laos">Laos</option>
                            <option value="Latvia">Latvia</option>
                            <option value="Lebanon">Lebanon</option>
                            <option value="Lesotho">Lesotho</option>
                            <option value="Liberia">Liberia</option>
                            <option value="Libya">Libya</option>
                            <option value="Liechtenstein">Liechtenstein</option>
                            <option value="Lithuania">Lithuania</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="Macau">Macau</option>
                            <option value="Macedonia">Macedonia</option>
                            <option value="Madagascar">Madagascar</option>
                            <option value="Malaysia">Malaysia</option>
                            <option value="Malawi">Malawi</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marshall Islands">Marshall Islands</option>
                            <option value="Martinique">Martinique</option>
                            <option value="Mauritania">Mauritania</option>
                            <option value="Mauritius">Mauritius</option>
                            <option value="Mayotte">Mayotte</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Midway Islands">Midway Islands</option>
                            <option value="Moldova">Moldova</option>
                            <option value="Monaco">Monaco</option>
                            <option value="Mongolia">Mongolia</option>
                            <option value="Montserrat">Montserrat</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Mozambique">Mozambique</option>
                            <option value="Myanmar">Myanmar</option>
                            <option value="Nambia">Nambia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Netherland Antilles">Netherland Antilles</option>
                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
                            <option value="Nevis">Nevis</option>
                            <option value="New Caledonia">New Caledonia</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigeria">Nigeria</option>
                            <option value="Niue">Niue</option>
                            <option value="Norfolk Island">Norfolk Island</option>
                            <option value="Norway">Norway</option>
                            <option value="Oman">Oman</option>
                            <option value="Pakistan">Pakistan</option>
                            <option value="Palau Island">Palau Island</option>
                            <option value="Palestine">Palestine</option>
                            <option value="Panama">Panama</option>
                            <option value="Papua New Guinea">Papua New Guinea</option>
                            <option value="Paraguay">Paraguay</option>
                            <option value="Peru">Peru</option>
                            <option value="Phillipines">Philippines</option>
                            <option value="Pitcairn Island">Pitcairn Island</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Puerto Rico">Puerto Rico</option>
                            <option value="Qatar">Qatar</option>
                            <option value="Republic of Montenegro">Republic of Montenegro</option>
                            <option value="Republic of Serbia">Republic of Serbia</option>
                            <option value="Reunion">Reunion</option>
                            <option value="Romania">Romania</option>
                            <option value="Russia">Russia</option>
                            <option value="Rwanda">Rwanda</option>
                            <option value="St Barthelemy">St Barthelemy</option>
                            <option value="St Eustatius">St Eustatius</option>
                            <option value="St Helena">St Helena</option>
                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                            <option value="St Lucia">St Lucia</option>
                            <option value="St Maarten">St Maarten</option>
                            <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                            <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                            <option value="Saipan">Saipan</option>
                            <option value="Samoa">Samoa</option>
                            <option value="Samoa American">Samoa American</option>
                            <option value="San Marino">San Marino</option>
                            <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                            <option value="Saudi Arabia">Saudi Arabia</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Serbia">Serbia</option>
                            <option value="Seychelles">Seychelles</option>
                            <option value="Sierra Leone">Sierra Leone</option>
                            <option value="Singapore">Singapore</option>
                            <option value="Slovakia">Slovakia</option>
                            <option value="Slovenia">Slovenia</option>
                            <option value="Solomon Islands">Solomon Islands</option>
                            <option value="Somalia">Somalia</option>
                            <option value="South Africa">South Africa</option>
                            <option value="Spain">Spain</option>
                            <option value="Sri Lanka">Sri Lanka</option>
                            <option value="Sudan">Sudan</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Swaziland">Swaziland</option>
                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Syria">Syria</option>
                            <option value="Tahiti">Tahiti</option>
                            <option value="Taiwan">Taiwan</option>
                            <option value="Tajikistan">Tajikistan</option>
                            <option value="Tanzania">Tanzania</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Togo">Togo</option>
                            <option value="Tokelau">Tokelau</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                            <option value="Turkmenistan">Turkmenistan</option>
                            <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="United Arab Erimates">United Arab Emirates</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States of America">United States of America</option>
                            <option value="Uraguay">Uruguay</option>
                            <option value="Uzbekistan">Uzbekistan</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Vatican City State">Vatican City State</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Vietnam">Vietnam</option>
                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                            <option value="Wake Island">Wake Island</option>
                            <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                            <option value="Yemen">Yemen</option>
                            <option value="Zaire">Zaire</option>
                            <option value="Zambia">Zambia</option>
                            <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                        <div className="input text-red"></div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div>
                      <div className='form-group'>
                      <label>City</label>
                        <input type="text"
                            name="city"
                            defaultValue={this.state.developper.city}
                            className="form-control"
                            placeholder="City"
                            ref="city"/>
                        <div className="input text-red"></div>
                      </div>
                      <div className="form-group">
                      <label>Skills</label>
                        <div>
                          <div className='form-group'>
                            <TagsInput inputProps={{className: 'react-tagsinput-input',placeholder: 'Add skills'}} value={this.state.skills} onChange={this.handleSkillsChange} />
                            <div className="input text-red"></div>
                          </div>
                        </div>
                      </div>
                      <div className='form-group'>
                      <label>Profile color</label>
                        <select
                          className="form-control"
                          defaultValue={this.state.developper.color}
                          id="color" 
                          name="color"
                          ref="color">
                            <option value="gray">gray</option>
                            <option value="red">red</option>
                            <option value="blue">blue</option>
                            <option value="Info">lightblue</option>
                            <option value="aqua">aqua</option>
                            <option value="teal">Teal</option>
                            <option value="purple">Purple</option>
                            <option value="orange">Orange</option>
                            <option value="yellow">Yellow</option>
                            <option value="black">Black</option>
                            <option value="maroon">Pink</option>
                            <option value="navy">Navy</option>
                        </select>
                        <div className="input text-red"></div>
                      </div>
                    </div>
                  </div>
                </ul>
              </div>
            </div>

            <div className="box box-widget widget-user-2 text-center">
              <div className="widget-user-header bg-default-active">
              <div className="form-group">
                <div>
                  <div className='form-group'>
                    <label><img  src="css/adminLTE/img/facebook-icon.png" alt="Pulpit Rock" width="35" className="logomargin"/></label>
                    <input type="text"
                        name="facebook"
                        defaultValue={this.state.developper.facebook}
                        className="form-control"
                        placeholder="Facebook Link"
                        ref="facebook"/>
                    <div className="input text-red"></div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div>
                  <div className='form-group'>
                    <label><img  src="css/adminLTE/img/twitter-icon.png" alt="Pulpit Rock" width="35" className="logomargin"/></label>
                    <input type="text"
                        name="twitter"
                        defaultValue={this.state.developper.twitter}
                        className="form-control"
                        placeholder="Twitter Link"
                        ref="twitter"/>
                    <div className="input text-red"></div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div>
                  <div className='form-group'>
                    <label><img  src="css/adminLTE/img/Linkedin.png" alt="Pulpit Rock" width="35" className="logomargin"/></label>
                    <input type="text"
                        name="linkedin"
                        defaultValue={this.state.developper.linkedin}
                        className="form-control"
                        placeholder="Linkedin Link"
                        ref="linkedin"/>
                    <div className="input text-red"></div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div>
                  <div className='form-group'>
                    <label><img  src="css/adminLTE/img/github1600.png" alt="Pulpit Rock" width="35" className="logomargin"/></label>
                    <input type="text"
                        name="git"
                        defaultValue={this.state.developper.git}
                        className="form-control"
                        placeholder="Git Link"
                        ref="git"/>
                    <div className="input text-red"></div>
                  </div>
                </div>
              </div>
              </div>
              
            </div>

          </div>

          <div className="col-md-8">
          <div className="nav-tabs-custom">
            <ul className="nav nav-tabs">
              <li className="active"><a>Overview</a></li>
              <li className="pull-right"><button onClick={this.update} type="button" className="btn btn-success">Save All Changes</button></li>
            </ul>
            <div className="tab-content">
              <div className="form-group">
                <b>Write a description about your self and what you do...</b>
                <br/><br/>
                  <div className={wrapperClassOverview}>
                    <textarea
                        name="overview"
                        className="form-control"
                        defaultValue={this.state.developper.overview}
                        placeholder="Overview.."
                        style={{resize:"none"}}
                        rows="6"
                        id="overview"
                        ref="overview"/>
                    <div className="input text-red">{this.state.errors.overview}</div>
                  </div>
              </div>
            </div>
          </div>
          
          <Realization idUser={this.props.params.id} />

          <div className="nav-tabs-custom">
            <ul className="nav nav-tabs">
              <li className="active"><a>Following</a></li>
            </ul>
            <div className="tab-content">
              
              <Follower followers={this.state.developper.following}/>

            </div>
          </div>
          
         </div>
        
        </div>
              <Modal show={this.state.showmodal}>
                <Modal.Body>
                  Updating.. <i className="fa fa-refresh fa-spin"></i>
                </Modal.Body>
              </Modal>
              <Modal show={this.state.showresult} onHide={this.showresultClose}>
                <Modal.Body>
                 <i className="fa fa-check "></i> Your profile was successfully updated 
                </Modal.Body>
              </Modal>

      </div>
    );
  }

}