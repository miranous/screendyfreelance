import React, { Component } from 'react';
import { Link } from 'react-router';
import DevelopperActions from '../actions/DevelopperActions';
import ComponentStore from '../stores/ComponentStore';

export default class developper extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      developper: this.props.developper
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      developper: nextProps.developper
    });
  }

  render() {

    var projects = <span><br /><br /><br /><br /><br /></span>;
    var count = 0;
    if(this.state.developper.projects.length > 0){
        projects = this.state.developper.projects.map((project) => {
        count++;
        if(count > 4)
          return ;
        if(project.type=="app")
        return <Link to={`AppDetails/${project.id}`} key={project.id}><span ><img  src={project.icon} alt="project" width="60" className="style_prevu_kit img-rounded"/>&nbsp;&nbsp;</span></Link>;
        else if(project.type=="template")
        return (<a key={project.id} href={`http://screendymarket.cloudapp.net:8080/MarketplaceSites/market/#/Templates/Details/${project.idProduct}`} target="_blank">
                  <img src={project.icon} width="60" className="img-rounded " width="60" className="style_prevu_kit img-rounded"/>&nbsp;&nbsp;
                </a>);
        
      });
  }
    return (
      
        <div>
          <div className="box box-widget widget-user">
              <div className={`widget-user-header bg-${this.state.developper.color}-active`}>
                <div className="pull-left">
                  <p><b>{this.state.developper.firstname} {this.state.developper.lastname}</b></p>
                  <h6 className="widget-user-desc"><b>{this.state.developper.description}</b></h6>
                </div>
                <div className="pull-right text-center">
                  <p><b>{this.state.developper.rank}</b></p>
                  <img  src={this.state.developper.rankavatar} height="50" width="50" data-toggle="tooltip" data-placement="right" title="rank" />
                </div>
              </div>
              <Link to={`Details/${this.state.developper.idUser}`}>
                <div className="widget-user-image">
                  <img className=" img-circle centerimage" width="100" src={this.state.developper.avatar} alt="User Avatar"/>
                </div>
              </Link>
            <div className="box-footer">
              <div className="row">
                <div className="col-sm-4 border-right">
                    <div className="description-block " >
                      <span  className="small" data-toggle="tooltip" data-placement="right" title="location"><i className="fa fa-map-marker margin-r-5"></i>{this.state.developper.city}</span>
                    </div>
                </div>

                <div className="col-sm-4 border-right">
                  <div className="description-block">
                    <span  className="description-text" data-toggle="tooltip" data-placement="right" title="followers"><i className="fa fa-users"></i> {this.state.developper.followers}</span>
                  </div>
                </div>
                
                <div className="col-sm-4">
                    <div className="description-block" >

                      <span data-toggle="tooltip" data-placement="right" title="projects"><i className="fa fa-folder"></i> {this.state.developper.numberprojects}</span>
                      
                    </div>
                </div>
              </div>
            </div>
            <div className="box box-gray text-center">
              <div>
              <br />
                   {projects}
                   <br /><br />
              </div>
            </div>
            
          </div>

        </div>

      
    );
  }

}