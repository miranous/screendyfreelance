import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';
import { Link } from 'react-router';

export default class Follower extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.state={
      followers : this.props.followers
    }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      followers : nextProps.followers
    });
  }
  createFollowers(follower){
    return (
     <div className="col-md-2 text-center" key={follower.id}>
       <Link to={`Details/${follower.id}`}>
       <img id={follower.id} src={follower.avatar} alt="Pulpit Rock" width="100" className="img-circle centerimagefollowers"/>
       </Link>
       <b>{follower.firstname}</b>
       <br />
       <b> {follower.lastname}</b>
       <br /><br />
     </div>
      );
  }
  render() {
    return (
     <div className="row">{this.state.followers.map(this.createFollowers, this)}</div>
    );
  }
}