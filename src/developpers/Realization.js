import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import Applications from './Applications';
import DevelopperActions from '../actions/DevelopperActions';
import ComponentStore from '../stores/ComponentStore';

export default class Realization extends Component {

	constructor(props, context) {
    	super(props, context);
    	DevelopperActions.getTemplates(this.props.idUser,1);
    	this.state = {
    			idUser : this.props.idUser,
    			showmodal : false,
    			type : "p",
    			templates : [],
    			loading : false,
    			isScreendy : "0",
    			removeConfirmation : false,
    			removeId : 0
    		};
    	this.openModal=this.openModal.bind(this);
    	this.closeModal=this.closeModal.bind(this);
    	this.handleType=this.handleType.bind(this);
    	this._onChange=this._onChange.bind(this);
    	this._save=this._save.bind(this);
    	this.handleClick=this.handleClick.bind(this);
    	this._remove=this._remove.bind(this);
    	this._openConfirmation = this._openConfirmation.bind(this);
	    this._closeConfirmation = this._closeConfirmation.bind(this);
  	}

  	_openConfirmation(id) {
		this.setState({
			removeConfirmation : true
		});
		this.state.removeId = id;
	}

	_closeConfirmation() {
		this.setState({
			removeConfirmation : false
		});
	}

  	_remove(id) {

  		var form_data = new FormData();
	  		form_data.append('idUser', parseInt(this.state.idUser));
	  		form_data.append('template', id);
	  		$.ajax({
	                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/removeTemplateFromFreelance',
	                              contentType: false,
	                              processData: false,
	                              cache: false,
	                              data: form_data,                   
	                              type: 'POST',
	                              success: function(response){
	                                  
	                              },
	                              error: function (xhr, ajaxOptions, thrownError) {
	                                
	                              }
	                   });
	  	DevelopperActions.getTemplates(this.props.idUser,1);
	  	DevelopperActions.getTemplates(this.props.idUser,1);
		DevelopperActions.getTemplates(this.props.idUser,1);
		this.setState({
			removeConfirmation : false
		});

  	}

  	handleClick(event){
  		if(event.target.id === "p")
  			this.setState({
	        	isScreendy : "0",
	        	loading : true
	        });
  		if(event.target.id === "s")
  			this.setState({
	        	isScreendy : "1",
	        	loading : true
	        });
  	}

  	_save(){

  		if(this.state.type === "p"){

  			var form_data = new FormData();
	  		form_data.append('idUser', this.props.idUser);
	  		form_data.append('name', this.refs.name.value);
	  		form_data.append('screendyapp', 0);
	  		form_data.append('description', this.refs.description.value);
	  		form_data.append('demo', this.refs.demo.value);
	  		form_data.append('screendyid', "");
	  		form_data.append('icon', this.refs.icon.files[0]);
	  		form_data.append('preview1', this.refs.preview1.files[0]);
	  		form_data.append('preview2', this.refs.preview2.files[0]);
	  		form_data.append('preview3', this.refs.preview3.files[0]);
	  		DevelopperActions.AddApp(form_data,this.props.idUser,0);

  		}

  		if(this.state.type === "s"){

  			var form_data = new FormData();
	  		form_data.append('idUser', this.props.idUser);
	  		form_data.append('name', this.refs.name.value);
	  		form_data.append('screendyapp', 1);
	  		form_data.append('description', this.refs.description.value);
	  		form_data.append('demo', this.refs.demo.value);
	  		form_data.append('screendyid', this.refs.screendyid.value);
	  		form_data.append('icon', this.refs.icon.files[0]);
	  		form_data.append('preview1', this.refs.preview1.files[0]);
	  		form_data.append('preview2', this.refs.preview2.files[0]);
	  		form_data.append('preview3', this.refs.preview3.files[0]);
	  		DevelopperActions.AddApp(form_data,this.props.idUser,1);

  		}
  		
  		if(this.state.type === "t"){

			var templatesforaddtmp = [];
  			for (var ref in this.refs) {
  				if(ref.startsWith("add")) {
  					if(this.refs[ref].checked){
  						var t = {
  							id : this.refs[ref].value
  						};
  						templatesforaddtmp.push(t);
  					}
  				}
  			}
  			var form_data = new FormData();
	  		form_data.append('idUser', parseInt(this.state.idUser));
	  		form_data.append('templates', JSON.stringify(templatesforaddtmp));
	  		$.ajax({
	                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/addTemplateToFreelance',
	                              contentType: false,
	                              processData: false,
	                              cache: false,
	                              data: form_data,                   
	                              type: 'POST',
	                              success: function(response){
	                                  
	                              },
	                              error: function (xhr, ajaxOptions, thrownError) {
	                                
	                              }
	                   });
	  		DevelopperActions.getTemplates(this.props.idUser,1);
	  		DevelopperActions.getTemplates(this.props.idUser,1);
			DevelopperActions.getTemplates(this.props.idUser,1);

  		}
  		this.setState({
        	loading : true
        });
  		this.closeModal();
  	}

  	componentWillMount(){
		ComponentStore.addChangeListener(this._onChange);
	}

	componentWillUnmount(){
	    ComponentStore.removeChangeListener(this._onChange);
	}

	_onChange() {
		this.setState({
        	templates: ComponentStore.getDevTemplates()
        });
	}

  	openModal() {
        this.setState({
        	showmodal: true
        });
      }

    closeModal() {
        this.setState({
        	showmodal: false,
        	type : "p"
        });
        DevelopperActions.getTemplates(this.props.idUser,1);
      }

    handleType() {
    	this.setState({
        	type : this.refs.type.value
        });
        if(this.refs.type.value === "t") {
        	DevelopperActions.getTemplates(this.props.idUser,0);
        	DevelopperActions.getTemplates(this.props.idUser,0);
			DevelopperActions.getTemplates(this.props.idUser,0);
        }
    }

    createTemplate(template){
    	return(
    		 <div key={template.id} className="col-md-2 text-center">
			     <img src={template.previews[0].urlPreview} alt="Pulpit Rock" height="200" width="120" className="logomargin"/>
			     <br />
			     <b >{template.componentName}</b>
			     <p ><input value={template.id} type="checkbox" ref={`add${template.id}`} className="minimal" /></p>
			     <br /><br />
		     </div>
    		);
    }

    createTemplate2(template){
    	return(
    		 <div key={template.id} className="col-md-3 text-center">
			     <img src={template.previews[0].urlPreview} alt="Pulpit Rock" height="200" width="120" className="logomargin"/>
			     <br />
			     <b >{template.componentName}</b>
			     <p ><a onClick={() => this._openConfirmation(template.id)}><i className="fa fa-fw fa-remove text-red"></i></a></p>
			     <br /><br />
		     </div>
    		);
    }

  	render() {

  		return(

  			<div>

  				<div className="input-group-btn">
	            <button type="button" onClick={this.openModal} className="btn btn-primary pull-right"><i className="fa fa-plus-circle"></i> Add a project</button>
	          </div>
	          <br/>
	          <div className="nav-tabs-custom">
	            <ul className="nav nav-tabs">
	                    <li className="active"><a id="p" onClick={this.handleClick} href="#tab_1" data-toggle="tab">Projects</a></li>
	                    <li><a id="s" href="#tab_2" onClick={this.handleClick} data-toggle="tab">Screendy Apps</a></li>
	                    <li><a id="t" href="#tab_3" onClick={this.handleClick} data-toggle="tab">Templates</a></li>
	            </ul>
	            <div className="tab-content">
	             <div className="tab-pane" id="tab_3">
	             	<div className="row">
	                	{this.state.templates.map(this.createTemplate2,this)}
	                </div>
	              </div>
	              <div className="tab-pane" id="tab_2">
	                <Applications isScreendy={this.state.isScreendy} loading={this.state.loading} idUser={this.state.idUser} />
	              </div>
	              <div className="tab-pane active" id="tab_1">
	                <Applications isScreendy={this.state.isScreendy} loading={this.state.loading} idUser={this.state.idUser} />
	              </div>
	            </div>
	          </div>


	          <Modal show={this.state.showmodal} onHide={this.closeModal} bsSize="large">
			        	<Modal.Header closeButton>
		                  <h4 className="modal-title" id="myModalLabel">Add Realization</h4>
		                </Modal.Header>
		                <Modal.Body>
		                  	<div className="row">
									<div className="box-body">
									<div className="col-md-3" />
									<div className="col-md-6">
										<div className="form-group">
						                      	<b>
						                        <select
						                          className="form-control"
						                          onChange={this.handleType}
						                          id="type" 
						                          name="type"
						                          ref="type">
						                            <option value="p">Project</option>
						                            <option value="s">Screendy App</option>
						                            <option value="t">Template</option>
						                        </select>
						                        </b>
						                  </div>
						            </div>

									</div>							
			{ this.state.type != 't' ?

							<div className="row content">
							<p className="col-md-12" >To add a personal or <b>ScreenDy</b> project, you have to fill in all the fields</p>
							  <div className="col-md-6">

								<div className="box box-primary">

							            		<div className="box-body">

															
															<div><div className="form-group">
																<label>Name</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="name"
															            className="form-control"
															            placeholder="Name"
															            ref="name"/>
															        </div>
																</div>
															</div>

															<div className="form-group">
																<label>Icon</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="icon"
												                        className="form-control"
												                        placeholder="Icon"
												                        ref="icon"/>
															        </div>
																</div>
															</div>

															

															<div className="form-group">
																	<label>Description</label>
																	<div>
																		<textarea
															            name="description"
															            className="form-control"
															            style={{resize:"none"}}
                        												rows="10"
															            placeholder="Description"
															            ref="description"/>
																	</div>
																</div>
															</div>

							            		</div>

							            	</div>
							            </div>
							            <div className="col-md-6">
											<div className="box box-primary">
							            		<div className="box-body">
							            		{ this.state.type === 's' ?
							            		<div className="form-group">
																<label>Screendy Project Name</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="screendyid"
															            className="form-control"
															            placeholder="Screendy Project Name"
															            ref="screendyid"/>
															        </div>
																</div>
															</div>
													:
												<div></div>
												}
													<div className="form-group">
																<label>Demo URL</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="demo"
															            className="form-control"
															            placeholder="Demo URL"
															            ref="demo"/>
															        </div>
																</div>
															</div>

							            			<div className="form-group">
																<label>Preview 1</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview1"
												                        className="form-control"
												                        placeholder="preview1"
												                        ref="preview1"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 2</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview2"
												                        className="form-control"
												                        placeholder="preview2"
												                        ref="preview2"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 3</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview3"
												                        className="form-control"
												                        placeholder="preview3"
												                        ref="preview3"/>
															        </div>
																</div>
															</div>

							            		</div>
							            	</div>
							            </div>
							            </div>
							            :
							            <div>
							            	{this.state.templates.map(this.createTemplate,this)}
							            </div>
							        }
							            </div>
		                </Modal.Body>
		                <Modal.Footer>
		                	<button type="button" className="btn btn-default pull-left" onClick={this.closeModal}>Cancel</button>
					        <button type="button" className="btn btn-primary" onClick={this._save}>Save</button>
		                </Modal.Footer>
		            </Modal>

		            <Modal show={this.state.removeConfirmation} onHide={this._closeConfirmation}>
		                <Modal.Body>
		                  	Do you want to remove ?
		                </Modal.Body>
		                <Modal.Footer>
		                	<button type="button" className="btn btn-default pull-left" onClick={this._closeConfirmation}>No</button>
					        <button type="button" className="btn btn-primary" onClick={() => this._remove(this.state.removeId)}>Yes</button>
		                </Modal.Footer>
		            </Modal>

  			</div>

  			);
  	}

}