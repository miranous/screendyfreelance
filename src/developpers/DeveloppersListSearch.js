import React, { Component } from 'react';
import Dev from './developper';
import DevelopperActions from '../actions/DevelopperActions';
import DevStore from '../stores/DevStore';
import { Link } from 'react-router';
import Row from './PaginationSearch';
export default class DeveloppersListSearch extends Component {

  constructor(props, context) {
    super(props, context);
    DevelopperActions.GetDeveloppersSearch(this.props.search);
    this.state = {
      developper : null,
      numberOfPages : 1,
      pages : [],
      rank : this.props.rank,
      loading : this.props.loading
    }
    this.createDev=this.createDev.bind(this);
    this.createPage=this.createPage.bind(this);
    this.onChange=this.onChange.bind(this);
    this.pagination=this.pagination.bind(this);
  }

  createDev(developper) {
   
    return (<div key={developper.idUser} className="col-md-4"><Dev developper={developper} /></div>);
   
  }

  createPage(page){

    return <Row key={page} i={page} search={this.props.search}/>;

  }

  onChange(){
    
      this.setState({
        developper : DevStore.getDevsByPage(this.props.page),
        numberOfPages : parseInt((DevStore.getDevs().length/9),10),
        loading : false
      });
      if((DevStore.getDevs().length%9)>0)
        this.setState({
        numberOfPages : this.state.numberOfPages+1
      });
    this.pagination();
  }

  componentWillMount(){
    DevStore.addChangeListener(this.onChange);
  }
  componentWillUnmount(){
    DevStore.removeChangeListener(this.onChange);
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      developper : DevStore.getDevsByPage(nextProps.page),
      rank : nextProps.rank,
      numberOfPages : parseInt((DevStore.getDevs().length/6),10)
    });
  }
  pagination(){
    var pages=[];
    this.setState({pages : []});
    for(var i=1;i<=this.state.numberOfPages;i++)
    {
      pages[i]=i;
    }
    this.setState({pages : pages});
    this.forceUpdate();
  }

  render() {
    if(this.state.loading)
      return (
        <div className="row">
         <section className="col-md-12">
            <div className="nav-tabs-custom">
                <ul className="nav nav-tabs pull-right">
                  <li className="pull-left header"> Freelancers</li>
                </ul>
          </div>
          <div className="text-center">
            <b className="text-center">Loading, please wait.. <i className="fa fa-refresh fa-spin"></i></b>
            <br />
            <br />
          </div>
        </section>
      </div>);
    if(this.state.developper==null|| this.state.developper.length<1)
      return (
        <div className="row">
         <section className="col-md-12">
            <div className="nav-tabs-custom">
                <ul className="nav nav-tabs pull-right">
                  <li className="pull-left header"> Freelancers</li>
                </ul>
          </div>
          <div className="text-center">
            <b className="text-center">No match !</b> 
            <br />
            <br />
          </div>
        </section>
      </div>);
    if(this.state.developper.length>0)
    return (
      <div className="row" >
         <section className="row">
              <div className="nav-tabs-custom">
                  <ul className="nav nav-tabs pull-right">
                    
                    <li className="pull-left header"> Freelancers</li>
                  </ul>
            </div> 
          </section>

          <div className="row" >
          {this.state.developper.map(this.createDev, this)}
          </div>

          <footer className="row">
          <div className="col-md-12 text-center">
            <ul className="pagination pagination-lg">
              {this.state.pages.map(this.createPage, this)}
            </ul>
          </div>
        </footer>
      </div>
    );
    else
      return <div/>;
  }

}