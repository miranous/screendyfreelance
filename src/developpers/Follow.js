import React from 'react';
import { render } from 'react-dom';
import DevelopperActions from '../actions/DevelopperActions';
import FreelancerStore from '../stores/FreelancerStore';

export default class Follow extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.state={
      idfollowed : this.props.idfollowed,
      idfollower : this.props.idfollower,
      followers : this.props.followers
    }
    this.Unfollow=this.Unfollow.bind(this);
    this.Follow=this.Follow.bind(this);
    }
  Follow(){
    if(this.state.idfollower)
      DevelopperActions.Follow(parseInt(this.state.idfollowed), parseInt(this.state.idfollower));
    else
      this.props.changeRoute(null, '/Signin');
  }
  Unfollow(){
    console.log('idfloowed ',this.state.idfollowed,' idfolower ',this.state.idfollower);
    DevelopperActions.UnFollow(parseInt(this.state.idfollowed), parseInt(this.state.idfollower));
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      idfollowed : nextProps.idfollowed,
      idfollower : nextProps.idfollower,
      followers : nextProps.followers
    });
  }
  ifFollowing(){
    for(var i=0;i<this.state.followers.length;i++)
    {
      if(this.state.idfollower === this.state.followers[i].id)
        return true;
    }
    return false;
  }
  render() {
    var button =null;
    if(this.state.idfollower != this.state.idfollowed)
    {
      button = <button onClick={this.Follow} type="button" className="btn btn-block btn-default"><span className="fa fa-user-plus"></span> Follow</button>;
    
      if(this.ifFollowing())
      {
        button=<button onClick={this.Unfollow} type="button" className="btn btn-block btn-danger"><span className="fa  fa-check "></span> Followed</button>
      }
    }
    
    return (
 
      <div>
      {button}
      </div>

    );
  }
}