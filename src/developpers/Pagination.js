import React, { Component } from 'react';
import { Link } from 'react-router';
export default class Pagination extends Component {

  constructor(props, context) {
    super(props, context);
    this.state={
    	i : this.props.i,
    	rank : this.props.rank
    }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      i : nextProps.i,
      rank : nextProps.rank
    });
  }
   
  render() {
    return (
     <li><Link to={`developpers/${this.state.i}/${this.state.rank}`}>{this.state.i}</Link></li>
    );
  }

}