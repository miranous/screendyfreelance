import React, { Component } from 'react';
import { Link } from 'react-router';
import DevelopperActions from '../actions/DevelopperActions';
import ComponentStore from '../stores/ComponentStore';
import UserStore from '../stores/UserStore';

export default class AppDetails extends Component {

  constructor(props, context) {
    super(props, context);
    DevelopperActions.GetAppDetails(this.props.params.id);
    this.state={
      Details : null,
      likes : '0'
    }
    this.onChange=this.onChange.bind(this);
    this.isLiked=this.isLiked.bind(this);
    this.Like=this.Like.bind(this);
    this.Dislike = this.Dislike.bind(this);
    this.isConnected=this.isConnected.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
    });
  }
  onChange(){
    this.setState({
      Details : ComponentStore.getAppDetails(),
      likes : ComponentStore.getAppDetails().Likes
    });
  }

  componentWillMount(){
    ComponentStore.addChangeListener(this.onChange);
  }

  componentWillUnmount(){
    ComponentStore.removeChangeListener(this.onChange);
  }
  isConnected(){
     if(UserStore.getConnectedId())
      return true;
      return false;
  }
  isLiked(){
   
      for(var i = 0 ; i < this.state.likes.length ; i++)
      {
        if(UserStore.getConnectedId() == this.state.likes[i].idUser)
        {
          return true;
        }
      }
      return false;
    
  }
  Like(){
    DevelopperActions.Like(this.state.Details.idApp, this.state.Details.idUser, UserStore.getConnectedId());
  }
  Dislike(){
    DevelopperActions.Dislike(this.state.Details.idApp, UserStore.getConnectedId());
  }
  render() {
    var button = null;
    if(this.isConnected())
      {
        if(this.isLiked())
            button = <p className="btn-group"><button className="btn btn-danger" onClick={this.Dislike}><i className="fa  fa-check "></i> Liked</button><button className="btn btn-danger">{this.state.likes.length}</button></p>;
        else 
            button = <p className="btn-group"><button className="btn btn-primary" onClick={this.Like}><i  className="fa  fa-heart"></i> Like</button><button className="btn btn-primary">{this.state.likes.length}</button></p>;
      }
    else
      {
         button = <Link to="/Signin"><p className="btn-group"><button className="btn btn-primary"><i className="fa  fa-heart"></i> Like</button><button className="btn btn-primary">{this.state.likes.length}</button></p></Link>; 
      }
    if(this.state.Details==null || this.state.Details.length<1)
    return <div/>
    return (
    <div className="container">
    <section className="content-header">
              <h4>
                Application details
              </h4>
              <ol className="breadcrumb">
                <li><Link to='home'><i className="fa fa-dashboard"></i>Freelancers</Link></li>
                <li>Details</li>
                <li className="active">Application</li>
              </ol>
          </section>
    <div className="content">

        <div className="col-md-8">
          <div className="box box-default">
            <div className="box-header with-border">
              <h3 className="box-title pull-left">{this.state.Details.name}</h3>
               <div className="pull-right">{button}</div>
            </div>
            <div className="box-body">
              <div className="row">
                <img src={this.state.Details.preview1} alt="Pulpit Rock" height="320" width="200" className="col-md-4" />
                <img src={this.state.Details.preview2} alt="Pulpit Rock" height="320" width="200" className="col-md-4"/>
                <img src={this.state.Details.preview3} alt="Pulpit Rock" height="320" width="200" className="col-md-4"/>
              </div>
              
              
            </div>
            <div className="box-footer">
            <p><b>Description</b></p>
              <p>{this.state.Details.description}</p>
            </div>
          </div>
        </div>

        <div className="col-md-4">

          <div className="box box-default" >
            <div className="box-header with-border">
              <h3 className="box-title">Demo</h3>
            </div>
            <div className="box-body">
              <iframe src={this.state.Details.demo} width="299px" height="587px" frameBorder="0" scrolling="no"></iframe>
            </div>
          </div>

          <div className="box box-default">
            <div className="box-header with-border">
              <h3 className="box-title">Developer</h3>
            </div>
            <div className="box-body">

              <div className="box-body box-profile  text-center">
                <Link to={`Details/${this.state.Details.idUser}`}>
                <img className="profile-user-img img-circle" src={this.state.Details.avatar} alt="User Avatar" />
                </Link>
                <br/><br/>
                <p><b>{this.state.Details.firstname} {this.state.Details.lastname}</b></p>
              </div>
             
            </div>
          </div>
        </div>

      </div>
    </div>
    );
  }

}