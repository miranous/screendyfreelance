import React, { Component } from 'react';
import Dev from './developper';
import DevelopperActions from '../actions/DevelopperActions';
import DevStore from '../stores/DevStore';
import Row from './Pagination';

export default class DeveloppersList extends Component {

  constructor(props, context) {
    super(props, context);
    DevelopperActions.GetDeveloppers(this.props.rank,"popular");
    this.state = {
      developper : null,
      numberOfPages : 1,
      pages : [],
      rank : this.props.rank,
      type : "popular"
    }
    this.createDev=this.createDev.bind(this);
    this.createPage=this.createPage.bind(this);
    this.onChange=this.onChange.bind(this);
    this.pagination=this.pagination.bind(this);
    this.handlerankChange=this.handlerankChange.bind(this);
    this.handleClick=this.handleClick.bind(this);
  }
  handleClick(event){
      if(event.target.id === "newest")
        this.setState({
            type : "newest"
          });
      if(event.target.id === "popular")
        this.setState({
            type : "popular"
          });
      DevelopperActions.GetDeveloppers(this.props.rank,event.target.id);
    }
  handlerankChange(event){
    this.setState({rank : this.refs.rank.value});
    DevelopperActions.GetDeveloppers(this.refs.rank.value,this.state.type);
    this.props.changeRoute(null, `Developpers/1/${this.refs.rank.value}`);
  }
  createDev(developper) {
   
    return (<div key={developper.idUser} className="col-md-4"><Dev developper={developper} /></div>);
   
  }
  createPage(page){
    return <Row key={page} i={page} rank={this.refs.rank.value}/>;
  }
  onChange(){
    
      this.setState({
        developper : DevStore.getDevsByPage(this.props.page),
        numberOfPages : parseInt((DevStore.getDevs().length/9),10)
      });
      if((DevStore.getDevs().length%9)>0)
        this.setState({
        numberOfPages : this.state.numberOfPages+1
      });
    this.pagination();
  }
  componentWillMount(){
    DevStore.addChangeListener(this.onChange);
  }
  componentWillUnmount(){
    DevStore.removeChangeListener(this.onChange);
  }
  componentWillReceiveProps(nextProps) {
    DevelopperActions.GetDeveloppers(nextProps.rank,this.state.type);
    this.setState({
      developper : DevStore.getDevsByPage(nextProps.page),
      rank : nextProps.rank
    });
  }
  pagination(){
    var pages=[];
    this.setState({pages : []});
    for(var i=1;i<=this.state.numberOfPages;i++)
    {
      pages[i]=i;
    }
    this.setState({pages : pages});
    this.forceUpdate();
  }
  onClick(event){
    console.log('hello');
  }
  render() {
    var onClick= this.onClick.bind(this);
    if(this.state.developper==null|| this.state.developper.length<1)
      return (<div className="row">
         <section className="col-md-12">
            <div className="nav-tabs-custom">
                <ul className="nav nav-tabs pull-right">
                  <li className="header">
                    <select className="form-control" ref='rank' defaultValue={this.state.rank}>
                      <option value="All">All</option>
                      <option value="Officer">Officer</option>
                      <option value="Captain">Captain</option>
                      <option value="General">General</option>
                    </select>
                  </li>
                  <li ><a href="#tab1" data-toggle="tab" name="Newest">Newest</a></li>
                  <li className="active"><a href="#tab1" data-toggle="tab" name="Popular">Popular</a></li>
                  
                  
                  <li className="pull-left header"> Freelancers</li>
                </ul>
          </div>
          <div className="text-center">
            <b className="text-center">Loading, please wait.. <i className="fa fa-refresh fa-spin"></i></b> 
            <br />
            <br />
          </div>
        </section>
      </div>);
    if(this.state.developper.length>0)
    return (
      <div className="row" >
       <section className="col-md-12">
            <div className="nav-tabs-custom">
                <ul className="nav nav-tabs pull-right">
                  <li className="header">
                    <select className="form-control" ref='rank' defaultValue={this.state.rank} onChange={this.handlerankChange}>
                      <option value="All">All</option>
                      <option value="Officer">Officer</option>
                      <option value="Captain">Captain</option>
                      <option value="General">General</option>
                    </select>
                  </li>
                  <li ><a id="newest" href="#tab1" data-toggle="tab" onClick={this.handleClick} name="Newest">Newest</a></li>
                  <li className="active"><a id="popular" href="#tab1" data-toggle="tab" onClick={this.handleClick} name="Popular">Popular</a></li>
                  
                  <li className="pull-left header"> Freelancers</li>
                </ul>
          </div> 
        </section>
        
        <div className="row" >
        {this.state.developper.map(this.createDev, this)}
        </div>

        <footer className="row">
          <div className="col-md-12 text-center">
            <ul className="pagination pagination-lg">
              {this.state.pages.map(this.createPage, this)}
            </ul>
          </div>
        </footer>
      </div>
    );
    else
      return <div/>;
  }

}