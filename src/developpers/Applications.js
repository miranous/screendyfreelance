import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import _ from 'lodash';
import DevelopperActions from '../actions/DevelopperActions';
import AppStore from '../stores/AppStore';
import AppDispatcher from '../dispatcher/AppDispatcher';
import UserConstants from '../constants/UserConstants';

export default class Applications extends Component {

	constructor(props, context) {
	    	super(props, context);
	    	
	    	this.state = {
	    		isScreendy : this.props.isScreendy,
	    		events : [],
	    		add : false,
	    		update : false,
	    		loading : this.props.loading,
	    		event : {},
	    		removeConfirmation : false,
	    		removeId : 0
	    	};
	    	if(this.props.isScreendy === "0")
	    		DevelopperActions.GetDevelopperApps(this.props.idUser,0);
	    	if(this.props.isScreendy === "1")
	    		DevelopperActions.GetDevelopperApps(this.props.idUser,1);

	    	this._openAdd = this._openAdd.bind(this);
	    	this._closeAdd = this._closeAdd.bind(this);
	    	this._openConfirmation = this._openConfirmation.bind(this);
	    	this._closeConfirmation = this._closeConfirmation.bind(this);
	    	this._openUpdate = this._openUpdate.bind(this);
	    	this._closeUpdate = this._closeUpdate.bind(this);
	    	this._save = this._save.bind(this);
	    	this._update = this._update.bind(this);
	    	this._remove = this._remove.bind(this);
	    	this._onChange = this._onChange.bind(this);
	}

	_openConfirmation(id) {
		this.setState({
			removeConfirmation : true
		});
		this.state.removeId = id;
	}

	_closeConfirmation() {
		this.setState({
			removeConfirmation : false
		});
	}

	componentWillReceiveProps(nextProps) {
	    this.setState({
	     loading : nextProps.loading,
	     isScreendy : nextProps.isScreendy
	    });
	    if(nextProps.isScreendy === "0")
	    	DevelopperActions.GetDevelopperApps(nextProps.idUser,0);
	    if(nextProps.isScreendy === "1")
	    	DevelopperActions.GetDevelopperApps(nextProps.idUser,1);
	  }

	_onChange(){
		this.setState({
			events : AppStore.getApps(),
			loading : false
		});
      console.log('apps list:: ', AppStore.getApps());
	  }

	  componentWillMount(){
	    AppStore.addChangeListener(this._onChange);
	  }

	  componentWillUnmount(){
	    AppStore.removeChangeListener(this._onChange);
	  }

	_openUpdate(id) {
		let ev = _.find(this.state.events, {id: id});
		this.setState({
			update : true,
			event : ev
		});
	}

	_closeUpdate() {
		this.setState({
			update : false,
			event : {}
		});
	}

	_openAdd() {
		this.setState({
			add : true
		});
	}

	_closeAdd() {
		this.setState({
			add : false
		});
	}

	_save() {

		this.setState({
			loading : true
		});
		var form_data = new FormData();
  		form_data.append('idUser', this.props.idUser);
  		form_data.append('name', this.refs.name.value);
  		form_data.append('description', this.refs.description.value);
  		form_data.append('demo', this.refs.demo.value);
  		form_data.append('screendyid', this.refs.screendyid.value);
  		form_data.append('icon', this.refs.icon.files[0]);
  		form_data.append('preview1', this.refs.preview1.files[0]);
  		form_data.append('preview2', this.refs.preview2.files[0]);
  		form_data.append('preview3', this.refs.preview3.files[0]);
  		DevelopperActions.AddApp(form_data,this.props.idUser,this.state.isScreendy);
		this._closeAdd();
	}

	_update() {
		this.setState({
			loading : true
		});
		var form_data = new FormData();
		form_data.append('id', this.state.event.id);
  		form_data.append('idUser', this.props.idUser);
  		form_data.append('name', this.refs.name.value);
  		form_data.append('description', this.refs.description.value);
  		form_data.append('demo', this.refs.demo.value);
  		form_data.append('screendyid', this.refs.screendyid.value);
  		form_data.append('icon', this.refs.icon.files[0]);
  		form_data.append('preview1', this.refs.preview1.files[0]);
  		form_data.append('preview2', this.refs.preview2.files[0]);
  		form_data.append('preview3', this.refs.preview3.files[0]);
  		form_data.append('iconbackup', this.state.event.icon);
  		form_data.append('preview1backup', this.state.event.preview1);
  		form_data.append('preview2backup', this.state.event.preview2);
  		form_data.append('preview3backup', this.state.event.preview3);
  		DevelopperActions.UpdateApp(form_data,this.props.idUser,this.state.isScreendy);
		this._closeUpdate();
	}

	_remove(id) {
		this.setState({
			loading : true
		});
		var form_data = new FormData();
  		form_data.append('idApp', id);
  		DevelopperActions.RemoveApp(form_data,this.props.idUser,this.state.isScreendy);
  		this.setState({
			removeConfirmation : false
		});
	}



	events(ev) {
		return(
				<tr key={ev.id}>
			        <td className="fixed-td-blue">{ev.name}</td>
			        <td className="fixed-td-blue">
			        	<div className="widget-user-image">
	                  		<img  height="42" width="42" className="img-circle" src={ev.icon} alt="User Avatar" />
	                	</div>
			        </td>
			        {this.state.isScreendy === "1" ?
			        <td className="fixed-td-blue">{ev.screendyid}</td>
			        : null
			    	}
			        <td>
			            <i onClick={() => this._openUpdate(ev.id)} className="fa fa-fw fa-edit text-yellow"></i>
			            <a onClick={() => this._openConfirmation(ev.id)}><i className="fa fa-fw fa-remove text-red"></i></a>
			    	</td>
				</tr>
			);
	}

	render() {

		var tbody = null;
		if(this.state.loading)
			tbody = <tr><td>Loading, please wait.. <i className="fa fa-refresh fa-spin"></i></td></tr>
		else
			tbody = this.state.events.map(this.events,this);

		return(
			<div>
				<div className="row">
			        <div className="col-xs-12">
			            <div className="box-body">
			              <table className="table table-hover ">
			              	<thead>
			                <tr>
			                  <th>Name</th>
			                  <th>Icon</th>
			                  {this.state.isScreendy === "1" ?
			                  <th>Screendy ID</th>
			                  : null
			              		}
			                  <th>Actions</th>
			                </tr>
			                </thead>
			                <tbody>
			                
			                {tbody}

			                </tbody>
			              </table>
			            </div>
			        </div>

			        <Modal show={this.state.add} onHide={this._closeAdd} bsSize="large">
			        	<Modal.Header closeButton>
		                  <h4 className="modal-title" id="myModalLabel">Add Screendy Application</h4>
		                </Modal.Header>
		                <Modal.Body>
		                  	<div className="row">

							            <div className="col-md-6">
								<div className="box box-primary">

							            		<div className="box-body">

															
															<div><div className="form-group">
																<label>Name</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="name"
															            className="form-control"
															            placeholder="Name"
															            ref="name"/>
															        </div>
																</div>
															</div>

															<div className="form-group">
																<label>Icon</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="icon"
												                        className="form-control"
												                        placeholder="Icon"
												                        ref="icon"/>
															        </div>
																</div>
															</div>

															

															<div className="form-group">
																	<label>Description</label>
																	<div>
																		<textarea
															            name="description"
															            className="form-control"
															            style={{resize:"none"}}
                        												rows="10"
															            placeholder="Description"
															            ref="description"/>
																	</div>
																</div>
															</div>

							            		</div>

							            	</div>
							            </div>
							            <div className="col-md-6">
											<div className="box box-primary">
							            		<div className="box-body">

							            		<div className="form-group">
																<label>Screendy Project ID</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="screendyid"
															            className="form-control"
															            placeholder="Screendy ID"
															            ref="screendyid"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Demo URL</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="demo"
															            className="form-control"
															            placeholder="Demo URL"
															            ref="demo"/>
															        </div>
																</div>
															</div>

							            			<div className="form-group">
																<label>Preview 1</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview1"
												                        className="form-control"
												                        placeholder="preview1"
												                        ref="preview1"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 2</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview2"
												                        className="form-control"
												                        placeholder="preview2"
												                        ref="preview2"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 3</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview3"
												                        className="form-control"
												                        placeholder="preview3"
												                        ref="preview3"/>
															        </div>
																</div>
															</div>

							            		</div>
							            	</div>
							            </div>
							            </div>
		                </Modal.Body>
		                <Modal.Footer>
		                	<button type="button" className="btn btn-default pull-left" onClick={this._closeAdd}>Close</button>
					        <button type="button" className="btn btn-primary" onClick={this._save}>Save</button>
		                </Modal.Footer>
		            </Modal>

		            <Modal show={this.state.update} onHide={this._closeUpdate} bsSize="large">
			        	<Modal.Header closeButton>
		                  <h4 className="modal-title" id="myModalLabel">Update an Event</h4>
		                </Modal.Header>
		                <Modal.Body>
		                  	<div className="row">
		                	<div className="col-md-6">
								<div className="box box-primary">

							            		<div className="box-body">

							            					<div className="form-group">
																<label>Name</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="name"
															            defaultValue = {this.state.event.name}
															            className="form-control"
															            placeholder="Name"
															            ref="name"/>
															        </div>
																</div>
															</div>

															<div className="form-group">
																<label>Icon</label>
																<div>
																    <div className="form-group">
															          <div className="widget-user-image">
												                  		<img  height="60" width="60" className="img-circle" src={this.state.event.icon} alt="User Avatar" />
												                	</div>
															        </div>
																</div>
															</div>

															<div className="form-group">
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="icon"
												                        className="form-control"
												                        placeholder="Icon"
												                        ref="icon"/>
															        </div>
																</div>
															</div>

															<div className="form-group">
																	<label>Description</label>
																	<div>
																		<textarea
															            name="description"
															            className="form-control"
															            defaultValue = {this.state.event.description}
															            style={{resize:"none"}}
                        												rows="10"
															            placeholder="Description"
															            ref="description"/>
																	</div>
																</div>

							            		</div>

							            	</div>
							            </div>

							            <div className="col-md-6">
											<div className="box box-primary">
							            		<div className="box-body">

							            		<div className="form-group">
																<label>Screendy Project ID</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="screendyid"
															            defaultValue = {this.state.event.screendyid}
															            className="form-control"
															            placeholder="Screendy ID"
															            ref="screendyid"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Demo URL</label>
																<div>
																    <div className="form-group">
															          <input type="text"
															            name="demo"
															            className="form-control"
															            defaultValue = {this.state.event.demo}
															            placeholder="Demo URL"
															            ref="demo"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 1</label>
																<div>
																    <div className="form-group">
															          <div className="widget-user-image">
												                  		<img  height="320" width="180" src={this.state.event.preview1} alt="User Avatar" />
												                	</div>
															        </div>
																</div>
															</div>

							            			<div className="form-group">
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview1"
												                        className="form-control"
												                        placeholder="preview1"
												                        ref="preview1"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 2</label>
																<div>
																    <div className="form-group">
															          <div className="widget-user-image">
												                  		<img  height="320" width="180" src={this.state.event.preview2} alt="User Avatar" />
												                	</div>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview2"
												                        className="form-control"
												                        placeholder="preview2"
												                        ref="preview2"/>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 3</label>
																<div>
																    <div className="form-group">
															          <div className="widget-user-image">
												                  		<img  height="320" width="180" src={this.state.event.preview3} alt="User Avatar" />
												                	</div>
															        </div>
																</div>
															</div>

													<div className="form-group">
																<label>Preview 3</label>
																<div>
																    <div className="form-group">
															          <input type="file"
												                        name="preview3"
												                        className="form-control"
												                        placeholder="preview3"
												                        ref="preview3"/>
															        </div>
																</div>
															</div>

							            		</div>
							            	</div>
							            </div>

							            </div>
		                </Modal.Body>
		                <Modal.Footer>
		                	<button type="button" className="btn btn-default pull-left" onClick={this._closeUpdate}>Close</button>
					        <button type="button" className="btn btn-primary" onClick={this._update}>Save</button>
		                </Modal.Footer>
		            </Modal>

		            <Modal show={this.state.removeConfirmation} onHide={this._closeConfirmation}>
		                <Modal.Body>
		                  	Do you want to remove ?
		                </Modal.Body>
		                <Modal.Footer>
		                	<button type="button" className="btn btn-default pull-left" onClick={this._closeConfirmation}>No</button>
					        <button type="button" className="btn btn-primary" onClick={() => this._remove(this.state.removeId)}>Yes</button>
		                </Modal.Footer>
		            </Modal>

			      </div>
			      <br /><br /><br /><br /><br /><br />
			      </div>
			);

	}
	}