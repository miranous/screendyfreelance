import React, { Component } from 'react';
import DevList from '../developpers/DeveloppersList';
import ComponentDev from './ComponentDev';
import DevelopperActions from '../actions/DevelopperActions';
import DevStore from '../stores/DevStore';
import UserStore from '../stores/UserStore';
import ComponentStore from '../stores/ComponentStore';
import { Modal } from 'react-bootstrap';
import _ from 'lodash';
import App from './app';
import Follow from './Follow';
import Follower from './Follower';
import Activity from './Activity';
import { Link } from 'react-router';
export default class Details extends Component {

  constructor(props, context) {
    super(props, context);
    DevelopperActions.GetDevelopperDetails(this.props.params.id);
    DevelopperActions.getTemplates(this.props.params.id,1);
    this.state={
      developper : {},
      projects : [],
      apps : [],
      oProject : false,
      project : {},
      hireme: false,
      loading : true,
      hasTemplates : false,
      hasApps : false,
      hasProjects : false,
      idfollower : null,
      activeTab : "tab-pane active",
      confirmation : false
    }
    this.onChange=this.onChange.bind(this);
    this.onChangeTemplate=this.onChangeTemplate.bind(this);
    this.handleHireMe=this.handleHireMe.bind(this);
    this.handleHireMeClose=this.handleHireMeClose.bind(this);
    this.sendMail=this.sendMail.bind(this);
    this.openProject=this.openProject.bind(this);
    this.closeProject=this.closeProject.bind(this);
    this.openPopup=this.openPopup.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    DevelopperActions.GetDevelopperDetails(nextProps.params.id);
    DevelopperActions.getTemplates(nextProps.params.id,1);
  }
  closeProject(){
    this.setState({
      oProject : false,
      project : {},
      activeTab : "active"
    });
  }

  openPopup(){
    var url = "https://www.facebook.com/sharer/sharer.php?u=http%3A//screendymarket.cloudapp.net%3A8080/MarketplaceSites/freelancer/%23/Details/"+this.state.developper.idUser;
    window.open(url,'name','width=600,height=400');
  }

  openProject(event){
    event.preventDefault();

    if(_.find(this.state.projects, {id: parseInt(event.target.id)}))
      this.setState({
        project : _.find(this.state.projects, {id: parseInt(event.target.id)})
      });
    else
      this.setState({
        project : _.find(this.state.apps, {id: parseInt(event.target.id)})
      });
    this.setState({
      oProject : true
    });
    return;
  }

  sendMail() {
      var form_data = new FormData();
      form_data.append('toEmail', this.state.developper.email);
      form_data.append('fromEmail', this.refs.email.value);
      form_data.append('subject', this.refs.subject.value);
      form_data.append('message', this.refs.message.value);
      $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/hireMe',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){
                                  
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                
                              }
                   });
      this.setState({
        hireme: false,
        confirmation :true
      });
  }

  handleHireMe(){
    this.setState({
      hireme: true
    });
  }

  handleHireMeClose(){
    this.setState({
      hireme: false,
      confirmation :false
    });
  }


  onChange(){
    this.setState({
      developper : DevStore.getDevDetails(),
      projects : DevStore.getDevDetails().projects,
      apps : DevStore.getDevDetails().screendyapps,
      loading : false,
      idfollower : UserStore.getConnectedId()
    });
      if(DevStore.getDevDetails().screendyapps.length>0)
        this.setState({
          hasApps: true
        });
      if(DevStore.getDevDetails().projects.length>0)
        this.setState({
          hasProjects: true
        });
    $('.nav-tabs a:first').tab('show');
  }
  onChangeTemplate(){
    if(ComponentStore.getDevTemplates().length>0)
    this.setState({
      hasTemplates: true
    });
  }
  componentWillMount(){

    DevStore.addChangeListener(this.onChange);
    ComponentStore.addChangeListener(this.onChangeTemplate);
  }
  componentWillUnmount(){
    DevStore.removeChangeListener(this.onChange);
    ComponentStore.removeChangeListener(this.onChangeTemplate);
  }

  componentWillUpdate () {
    window.scrollTo(0, 0);

  }

  getMarkup() {
      return this.state.developper.overview;
    }

  render() {
    var templatesTab=null;
    var projectsTab=null;
    var appsTab=null;
    var activeProjects = null;
    var activeApps = null;
    var activeTemplates = null;

    
    
    if(this.state.hasProjects){
      projectsTab= <li ><a href="#tab_3" data-toggle="tab">Applications</a></li>;
    }
    
    if(this.state.hasApps){
      appsTab= <li ><a href="#tab_5" data-toggle="tab">Screendy Apps</a></li>;
    }

    if(this.state.hasTemplates){
      templatesTab= <li className="active"><a href="#tab_4" data-toggle="tab">Templates</a></li>;
    }
    var count2=0;
    var projects = null;
    if(this.state.projects.length > 0){
      projects = this.state.projects.map((project) => {
         
            return <App key={project.id} project={project}/>
         
        });
    }

    var apps = null;
    if(this.state.apps.length > 0){
      apps = this.state.apps.map((app) => {
         
            return <App key={app.id} project={app}/>
         
        });
    }

    var urlShare = "https://www.facebook.com/sharer/sharer.php?u=http%3A//screendymarket.cloudapp.net%3A8080/MarketplaceSites/freelancer/%23/Details/"+this.state.developper.idUser;

    var facebook = <a href={this.state.developper.facebook} target="_blank"><img  src="css/adminLTE/img/facebook-icon.png" alt="Pulpit Rock" width="35" className="logomargin"/></a>;
    var twitter =  <a href={this.state.developper.twitter} target="_blank"> <img  src="css/adminLTE/img/twitter-icon.png" alt="Pulpit Rock" width="40" className="logomargin"/></a>;
    var linkedin = <a href={this.state.developper.linkedin} target="_blank"> <img src="css/adminLTE/img/Linkedin.png" alt="Pulpit Rock" width="40" className="logomargin"/></a>;
    var git = <a href={this.state.developper.git} target="_blank"> <img src="css/adminLTE/img/github1600.png" alt="Pulpit Rock" width="42" className="logomargin"/></a>;
    if(this.state.developper.facebook === "" || this.state.developper.facebook === null)
      facebook = null;
    if(this.state.developper.twitter === "" || this.state.developper.twitter === null)
      twitter = null;
    if(this.state.developper.linkedin === "" || this.state.developper.linkedin === null)
      linkedin = null;
    if(this.state.developper.git === "" || this.state.developper.git === null)
      git = null;

    var skills = null;
    if(this.state.developper.skills) {
      var count=0;
    var skls = JSON.parse(this.state.developper.skills);
    skills = skls.map((skill) => {
        count++;
        if((count%4) == 0)
          return <span key={skill}><span className="label label-primary">{skill}</span>&nbsp;&nbsp;<br/></span>;
        return <span key={skill}><span className="label label-primary">{skill}</span>&nbsp;&nbsp;</span>;
      });
  }
    if(this.state.loading)
      return <div className="text-center">
            <b className="text-center">Loading, please wait.. <i className="fa fa-refresh fa-spin"></i></b> 
            <br />
            <br />
          </div>;
    return (
      <div className="container">
        <section className="content-header">
              <h4>
                Profile details
              </h4>
              <ol className="breadcrumb">
                <li><Link to='home'><i className="fa fa-dashboard"></i>Freelancers</Link></li>
                <li className="active">Details</li>
              </ol>
          </section>
        <section className="content">
       
          <div className="col-md-4">
            <div className="box box-primary">
              <div className={`box-body box-profile bg-${this.state.developper.color}-active text-center`}>
                <img className="profile-user-img img-circle centerimage" src={this.state.developper.avatar} alt="User Avatar" />
                <br/>
                <p><b>{this.state.developper.firstname} {this.state.developper.lastname}</b></p>
                <h5 className="widget-user-desc">{this.state.developper.description}</h5>
              </div>
              <div className="box-body">
                <div className="col-md-12">
                   <strong><i className="fa fa-map-marker margin-r-5"></i> Location</strong>

                    <p className="text-muted">{this.state.developper.city}, {this.state.developper.country}</p>

                </div>

                <div className="col-md-12">
                   <strong><i className="fa fa-pencil margin-r-5"></i> Skills</strong>
                    <p>{skills}</p>

                </div>

              </div>
            </div>
            <div className="box box-widget widget-user-2">
              <div className="widget-user-header bg-default-active">
                    <label><i className="fa fa-paragraph"></i> Overview</label>
                    <br />
                    <div dangerouslySetInnerHTML={{__html: this.getMarkup()}} />
              </div>
              
            </div>
            <div className="box box-widget widget-user-2 text-center">
              <div className="widget-user-header bg-default-active">
                    <label>{this.state.developper.rank}</label>
                    <br />
                    <img src={this.state.developper.rankavatar} width="80" />
              </div>
              
            </div>

            <div className="box box-widget widget-user-2 text-center">
              <div className="widget-user-header bg-default-active">
              <b className="text-center">You can find me here </b><br/>
                    {facebook}
                    {linkedin}
                    {twitter}
                    {git}
              </div>
              
            </div>

          </div>

          <div className="col-md-8">
          <div className="nav-tabs-custom">
            <ul className="nav nav-tabs" >
              <li className="active" ref="projects"><a href="#tab_1" data-toggle="tab">Projects</a></li>
              <li ><a href="#tab_2" data-toggle="tab">Followers</a></li>
              <li ><a href="#tab_6" data-toggle="tab">Activity</a></li>
              <li className="pull-right"><button type="button" className="btn btn-block btn-default"><a href={urlShare} onClick={this.openPopup} target="popup"><i className="fa fa-facebook"></i> Share</a></button></li>
              <li className="pull-right"><Follow idfollowed={this.state.developper.idUser} idfollower={this.state.idfollower} followers={this.state.developper.followers} changeRoute={this.props.history.pushState}/></li>
              <li className="pull-right"><button onClick={this.handleHireMe} type="button" className="btn btn-block btn-primary">Hire me</button></li>
            </ul>

            <div className="tab-content">
              <div className="tab-pane" id="tab_6">
                <Activity activities={this.state.developper.activities}/>
              </div>
              <div className="tab-pane" id="tab_2">
                <Follower followers={this.state.developper.followers}/>
              </div>

              <div className="tab-pane active" id="tab_1">
                <div className="nav-tabs-custom">

                  <ul className="nav nav-tabs">
                    <li ><a href="#tab_4" data-toggle="tab">Templates</a></li>
                    <li className="active"><a href="#tab_3" data-toggle="tab">Applications</a></li>
                    <li ><a href="#tab_5" data-toggle="tab">Screendy Apps</a></li>
                    
                  </ul>

                  <div className="tab-content">

                    <div className="tab-pane" id="tab_4">
                    {this.state.hasTemplates ?
                       <ComponentDev id={this.state.developper.idUser}/>
                       :
                       <p className="text-center">This user has no screendy templates</p>
                    }
                    </div>

                    <div className="tab-pane active" id="tab_3">
                      <div className="row">
                      {this.state.hasProjects ?
                       projects
                       :
                       <p className="text-center">This user has no projects</p>
                      }
                      </div>
                    </div>
                    <div className="tab-pane" id="tab_5">
                      <div className="row">
                      {this.state.hasApps ?
                       apps
                       :
                       <p className="text-center">This user has no screendy applications</p>
                      }
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>
          

          
         </div>
        </section>
              <Modal show={this.state.confirmation} onHide={this.handleHireMeClose}>
                <Modal.Header>
                  <h4 className="modal-title" id="myModalLabel">Email confirmation</h4>
                </Modal.Header>
                <Modal.Body>
                An email was successfully sent to the developer
                </Modal.Body>
              </Modal>
              <Modal show={this.state.hireme} onHide={this.handleHireMeClose}>
              
                   <div className="box box-primary">
                    <div className="box-header">
                      <i className="fa fa-envelope"></i>

                      <h3 className="box-title">Hire me</h3>
                      <div className="pull-right box-tools">
                        <button type="button" onClick={this.handleHireMeClose} className="btn btn-primary btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                          <i className="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div className="box-body">
                      <form action="#" method="post">
                        <div className="form-group">
                                <label>Your email</label>
                                <div>
                                    <div className="form-group">
                                        <input type="text"
                                          name="email"
                                          className="form-control"
                                          placeholder="Email"
                                          ref="email"/>
                                      </div>
                                </div>
                              </div>
                        <div className="form-group">
                                <label>Subject</label>
                                <div>
                                    <div className="form-group">
                                        <input type="text"
                                          name="subject"
                                          className="form-control"
                                          placeholder="Subject"
                                          ref="subject"/>
                                      </div>
                                </div>
                              </div>

                              <div className="form-group">
                                <label>Message</label>
                                <div>
                                    <div className="form-group">
                                        <textarea
                                          name="message"
                                          className="form-control"
                                          placeholder="Message"
                                          style={{resize:"none"}}
                                          rows="12"
                                          ref="message"/>
                                      </div>
                                </div>
                              </div>
                      </form>
                    </div>
                    <div className="box-footer clearfix">
                      <button type="button" className="btn btn-default pull-left" onClick={this.handleHireMeClose}>Cancel</button>
                      <button type="button" className="btn btn-primary pull-right" onClick={this.sendMail}>Send</button>
                    </div>
                  </div>

              </Modal>

      </div>
    );
  }

}