import UserStore from '../stores/UserStore';

function redirectToLogin(nextState, replaceState) {
  if (!UserStore.isSignedIn()) {
    replaceState({
      nextPathname: nextState.location.pathname
    }, '/login')
  }
}

function redirectToDashboard(nextState, replaceState) {
  if (UserStore.isSignedIn()) {
    replaceState(null, '/')
  }
}

export default {
  component: require('../containers/App'),
  childRoutes: [
    { 
      path: '/logout',
      getComponent: (location, cb) => {
        require.ensure([], (require) => {
          cb(null, require('../containers/Signout'))
        })
      }
    },
    { 
      path: '/about',
      getComponent: (location, cb) => {
        require.ensure([], (require) => {
          cb(null, require('../containers/About'))
        })
      }
    },
    { 
      onEnter: redirectToDashboard,
      childRoutes: [
        { 
          path: '/login',
          getComponent: (location, cb) => {
            require.ensure([], (require) => {
              cb(null, require('../containers/Signin'))
            })
          }
        }
      ]
    },
    { 
      onEnter: redirectToLogin,
      childRoutes: [
        // Protected routes that don't share the dashboard UI
        { 
          path: '/user/:id',
          getComponent: (location, cb) => {
            require.ensure([], (require) => {
              cb(null, require('../containers/User'))
            })
          }
        }
      ]
    },
    { 
      path: '/',
      getComponent: (location, cb) => {
        // Share the path
        // Dynamically load the correct component
        if (UserStore.isSignedIn()) {
          return require.ensure([], (require) => {
            cb(null, require('../containers/Dashboard'))
          })
        }
        return require.ensure([], (require) => {
          cb(null, require('../containers/Signin'))
        })
      },
      indexRoute: {
        onEnter: (nextState, replaceState) => {
          if (!UserStore.isSignedIn()) {
            replaceState({
              nextPathname: nextState.location.pathname
            }, '/login');
          }else {
            replaceState({
              nextPathname: nextState.location.pathname
            }, '/dashboard');
          }
        },
        getComponent: (location, cb) => {
          // Only load if we're logged in
          // if (UserStore.isSignedIn()) {
          //   return require.ensure([], (require) => {
          //     cb(null, require('../containers/Projects-dashboard'))
          //   })
          // }
          return cb()
        }
      },
      childRoutes: [
        { 
          onEnter: redirectToLogin,
          childRoutes: [
            // Protected nested routes for the dashboard
            { 
              path: '/dashboard',
              getComponent: (location, cb) => {
                require.ensure([], (require) => {
                  cb(null, require('../containers/Projects-dashboard'))
                })
              }
            }
          ]
        },
        { 
          onEnter: redirectToLogin,
          childRoutes: [
            // Protected nested routes for the dashboard
            { 
              path: '/home',
              getComponent: (location, cb) => {
                require.ensure([], (require) => {
                  cb(null, require('../components/Page'))
                })
              }
            }
          ]
        }
      ]
    }
  ]
}
