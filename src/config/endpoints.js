const BASE_URL  = 'http://screendymarket.cloudapp.net:8080/Marketplace/signin?';
const DEVS_URL  = 'http://screendymarket.cloudapp.net:8080/Marketplace/getFreelancersByRank?rank=';
const DEVS_URL_TOP3  = 'http://screendymarket.cloudapp.net:8080/Marketplace/getFreelancersByRank?rank=all&type=featured';
const DEVS_URL_SEARCH  = 'http://screendymarket.cloudapp.net:8080/Marketplace/getFreelancersBySearch?search=';
const IS_ACTIVE  = 'http://screendymarket.cloudapp.net:8080/Marketplace/isSessionActive';
const IS_FREELANCER  = 'http://screendymarket.cloudapp.net:8080/Marketplace/isFreelance?idDev';
const IS_COMPANY  = 'http://screendymarket.cloudapp.net:8080/Marketplace//isCompany?idDev';
const SIGN_OUT  = 'http://screendymarket.cloudapp.net:8080/Marketplace/logout';
const GET_DEV_DETAILS = 'http://screendymarket.cloudapp.net:8080/Marketplace/getFreelanceById?idDev=';
const GET_DEV_APPS = 'http://screendymarket.cloudapp.net:8080/Marketplace/getAppsByUser?idDev=';
const DEV_DATA = 'http://screendymarket.cloudapp.net:8080/Marketplace/getComponentsByUser/' ;
const FOLLOW = 'http://screendymarket.cloudapp.net:8080/Marketplace/followme?user=';
const GET_TEMPLATES = 'http://screendymarket.cloudapp.net:8080/Marketplace/getTemplatesByUser?idDev=';
const GET_APP_DETAILS = 'http://screendymarket.cloudapp.net:8080/Marketplace/getAppDetails?idApp=';
const SIGNUP = 'http://screendymarket.cloudapp.net:8080/Marketplace/signup?';
const JOBS = 'http://screendymarket.cloudapp.net:8080/Marketplace/getJobs';
const JOB_PROFIL = 'http://screendymarket.cloudapp.net:8080/Marketplace/getJobProfil?idUser=';

export const UserEP = {

	COMPANY_PROFIL(idcompany){
		return JOB_PROFIL + idcompany ;
	},

	SIGN_UP(userObject = {}) {
		return SIGNUP + 'email=' + userObject.email + '&password=' + userObject.password + '&passwordConfirmation=' + userObject.passwordConfirmation + '&firstname=' + userObject.firstName + '&lastname=' + userObject.lastName;
	},

	SIGN_IN(userObject = {}) {
		return BASE_URL + 'email=' + userObject.email 
								  + '&password=' + userObject.password;
	},

	GET_DEVS(rank,type){
		return DEVS_URL + rank + "&type=" + type;
	},

	GET_DEVS_TOP3(){
		return DEVS_URL_TOP3;
	},

	GET_DEVS_SEARCH(search){
		return DEVS_URL_SEARCH + search;
	},


	IS_ACTIVE() {
		return IS_ACTIVE;
	},

	SIGN_OUT() {
		return SIGN_OUT;
	},

	IS_FREELANCER(id){
		return IS_FREELANCER + '=' + id;
	},

	IS_COMPANY(id){
		return IS_COMPANY + '=' + id;
	},

	GET_DEV_DETAILS(id){
		return GET_DEV_DETAILS + id;
	},

	GET_DEV_APPS(id,screendy){
		return GET_DEV_APPS + id + "&screendy=" + screendy ;
	},

	DEV_DATA(id){
		return DEV_DATA + id;
	},

	GET_TEMPLATES(id,freelance){
		return GET_TEMPLATES + id + "&freelance=" + freelance;
	},

	FOLLOW(idfollowed, idfollower){
		return FOLLOW + idfollowed + '&follower=' + idfollower;
	},

	GET_APP_DETAILS(id){
		return GET_APP_DETAILS + id;
	},

	GET_ALL_JOBS(){
		return JOBS;
	}

}