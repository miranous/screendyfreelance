import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

import ApiHelper from '../utils/api';

/*
** User Store
*/


class UserStore extends BaseStore {

  constructor () {
    super();
    this.state = {
            _isSignedIn : false,
            company : '',
            companyAccount : '',
            email : '',
            firstName : '',
            lastName : '',
            id : '',
            plan : '',
            profil : '',
            photo : '',
            message : ''
    }
  }

  register (action) {
    switch(action.actionType) {
      
      case UserConstants.SIGN_IN:
        this.state = {
            _isSignedIn : true,
            company : action.userObject.user.company,
            companyAccount : action.userObject.user.companyAccount,
            email : action.userObject.user.email,
            firstName : action.userObject.user.firstName,
            lastName : action.userObject.user.lastName,
            id : action.userObject.user.idUser,
            plan : action.userObject.user.plan,
            profil : action.userObject.user.profil,
            photo : action.userObject.user.photo,
            message : ''
        }
        this.emitChange();
      break;

      case UserConstants.SIGN_IN_FAIL:
        this.state = {
            _isSignedIn : false,
            company : '',
            companyAccount : '',
            email : '',
            firstName : '',
            lastName : '',
            id : '',
            plan : '',
            profil : '',
            photo : '',
            message : action.message
        }
        this.emitChange();
      break;

      case UserConstants.IS_ACTIVE:

        if(action.userObject.isActive) {
            this.state = {
              _isSignedIn : true,
              company : action.userObject.data.company,
              companyAccount : action.userObject.data.companyAccount,
              email : action.userObject.data.email,
              firstName : action.userObject.data.firstName,
              lastName : action.userObject.data.lastName,
              id : action.userObject.data.idUser,
              plan : action.userObject.data.plan,
              profil : action.userObject.data.profil,
              photo : action.userObject.data.photo,
              message : ''
            }
        }
        else
          this.state = {
              _isSignedIn : false,
              company : '',
              companyAccount : '',
              email : '',
              firstName : '',
              lastName : '',
              id : '',
              plan : '',
              profil : '',
              photo : '',
              message : ''
            }

        this.emitChange();
      break;

      case UserConstants.SIGN_OUT:

        this.state = {
              _isSignedIn : false,
              company : '',
              companyAccount : '',
              email : '',
              firstName : '',
              lastName : '',
              id : '',
              plan : '',
              profil : '',
              photo : '',
              message : ''
            }
        this.emitChange();
      break;

    }
  }

  getUser() {
    return this.state;
  }

  isSignedIn() {
    return this.state._isSignedIn;
  }

  getConnectedId(){
    return this.state.id;
  }

}

export default new UserStore;
