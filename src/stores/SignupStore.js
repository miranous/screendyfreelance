import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

import ApiHelper from '../utils/api';

/*
** signup Store
*/


class SignupStore extends BaseStore {

  constructor () {
  super();
  this.state={
        myresponse : null
      
    }
  }
  SetisSignedUp(action){
    this.state={myresponse : action.message}
  }
  isSignedUp(){
    return this.state.myresponse;
  }
  register (action) {
    switch(action.actionType) {
      case UserConstants.SIGN_UP:
        this.SetisSignedUp(action);
        this.emitChange();
      break;
      case UserConstants.SIGN_UP_FAIL:
        this.SetisSignedUp(action);
        this.emitChange();
      break;
    }
  }
     
}

export default new SignupStore;
