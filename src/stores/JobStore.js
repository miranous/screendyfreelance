import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

class JobStore extends BaseStore {

  constructor () {
    super();
    this._initState=this._initState.bind(this);
    this._initState();
  }
  _initState(options) {
    this.state = {
      jobs : null
    };
  }

  register (action) {
    switch(action.actionType) {
      
      case UserConstants.GET_ALL_JOBS:
        this.setJobs(action.data.content);
        this.emitChange();
      break;

    }
  }
  setJobs(data){
    this.state={
      jobs : data
    }
  }
  getJobs(){
    return this.state.jobs;
  }

}

export default new JobStore;