import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

import ApiHelper from '../utils/api';


class CompanyStore extends BaseStore {

  constructor () {
    super();
    this.state = {
            _isCompany : false,
            _review : false,
            namecompany: '',
            avatar : '',
            location: '',
            description: '',
            email: ''
    }
  }

  register (action) {
    switch(action.actionType) {

      case UserConstants.IS_COMPANY:

        if(action.userObject.isCompany) {
          if(action.userObject.review) {
              this.state = {
                _isCompany : true,
                _review : true,
                namecompany: action.userObject.namecompany,
                avatar : action.userObject.logocompany,
                location: action.userObject.locationcompany,
                description: action.userObject.descriptioncompany,
                email: action.userObject.emailcompany
              }
          }else
            this.state = {
                _isCompany : true,
                _review : false,
                namecompany: action.userObject.namecompany,
                avatar : action.userObject.logocompany,
                location: action.userObject.locationcompany,
                description: action.userObject.descriptioncompany,
                email: action.userObject.emailcompany
            }
        }
        else
          this.state = {
              _isCompany : false,
              _review : false,
              namecompany: '',
              avatar : '',
              location: '',
              description: '',
              email: ''
            }

        this.emitChange();
      break;

      case UserConstants.COMPANY_PROFIL:

          this.state = {
                _isCompany : true,
                _review : true,
                namecompany: action.data.content.company.namecompany,
                avatar : action.data.content.company.logocompany,
                location: action.data.content.company.locationcompany,
                description: action.data.content.company.descriptioncompany,
                email: action.data.content.company.emailcompany
            }

        this.emitChange();
      break;

    }
  }

  getCompany() {
    return this.state;
  }

  isCompany(){
    return this.state._isCompany;
  }
  
  isReview() {
    return this.state._review;
  }

}

export default new CompanyStore;