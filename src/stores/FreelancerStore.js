import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

import ApiHelper from '../utils/api';


class FreelancerStore extends BaseStore {

  constructor () {
    super();
    this.state = {
            _isFreelancer : false,
            _review : false,
            skills: '',
            linkedin : '',
            git: '',
            lastname: '',
            firstname: '',
            avatar: '',
            country: '',
            city: '',
            rank: '',
            overview: '',
            email: '',
            description: '',
            facebook: '',
            idUser: 0,
            rankavatar: '',
            followed: false
    }
  }

  register (action) {
    switch(action.actionType) {

      case UserConstants.IS_FREELANCER:

        if(action.userObject.isFreelance) {
          if(action.userObject.review) {
              this.state = {
                _isFreelancer : true,
                _review : true,
                skills: '',
                linkedin : '',
                git: '',
                lastname: '',
                firstname: '',
                avatar: '',
                country: '',
                city: '',
                rank: '',
                overview: '',
                email: '',
                description: '',
                facebook: '',
                idUser: 0,
                rankavatar: ''
              }
          }else
            this.state = {
                _isFreelancer : true,
                _review : false,
                skills: '',
                linkedin : '',
                git: '',
                lastname: action.userObject.lastname,
                firstname: action.userObject.firstname,
                avatar: action.userObject.avatar,
                country: '',
                city: '',
                rank: '',
                overview: '',
                email: action.userObject.email,
                description: '',
                facebook: '',
                idUser: 0,
                rankavatar: ''
            }
        }
        else
          this.state = {
              _isFreelancer : false,
              _review : false,
              skills: '',
              linkedin : '',
              git: '',
              lastname: '',
              firstname: '',
              avatar: '',
              country: '',
              city: '',
              rank: '',
              overview: '',
              email: '',
              description: '',
              facebook: '',
              idUser: 0,
              rankavatar: '',
            }

        this.emitChange();
      break;


    }
  }

  getFreelancer() {
    return this.state;
  }

  isFreelancer() {
    return this.state._isFreelancer;
  }

  isReview() {
    return this.state._review;
  }

}

export default new FreelancerStore;