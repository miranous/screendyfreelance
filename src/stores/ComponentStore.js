import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

class ComponentStore extends BaseStore {

  constructor () {
    super();
    this._initState=this._initState.bind(this);
    this._initState();
  }
  _initState(options) {
    this.state = {
      devTemplates : [],
      appDetails : []
    };
  }

  setDevTemplates(data){
    this.state = {
      devTemplates : data
    }
  }

  getDevTemplates(){
    return this.state.devTemplates;
  }

  setAppDetails(data){
    this.state = {
      appDetails : data
    }
  }

  getAppDetails(){
    return this.state.appDetails;
  }

  register (action) {
    switch(action.actionType) {
      

      case UserConstants.DEV_DATA:
        this.setDevTemplates(action.data.content.Templates);
        this.emitChange();
      break;
      case UserConstants.DEV_TEMPLATES:
        this.setDevTemplates(action.data.content);
        this.emitChange();
      break;
      case UserConstants.GET_APP_DETAILS:
        this.setAppDetails(action.data.content);
        this.emitChange();
      break;

    }
  }

}

export default new ComponentStore;