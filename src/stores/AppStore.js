import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

class AppStore extends BaseStore {

  constructor () {
    super();
    this.setApps=this.setApps.bind(this);
    this._initState=this._initState.bind(this);
    this._initState();
  }
  _initState(options) {
    this.state = {
      apps: []
    };
  }
  setApps(data){
    this.state = {
      apps : data
    }
  }

  getApps(){
    return this.state.apps;
  }
  
  register (action) {
    switch(action.actionType) {
      case UserConstants.GET_DEV_APPS:
        this.setApps(action.userObject.content);
        this.emitChange();
      break;
      case UserConstants.ADD_DEV_APP:
        
        this.emitChange();
      break;

    }
  }

}

export default new AppStore;