import AppDispatcher from '../dispatcher/AppDispatcher';
import _ from 'lodash';

import BaseStore from './BaseStore';

import UserConstants from '../constants/UserConstants';

class DevStore extends BaseStore {

  constructor () {
    super();
    this.setDevs=this.setDevs.bind(this);
    this._initState=this._initState.bind(this);
    this._initState();
  }
  _initState(options) {
    this.state = {
      developpers: [],
      details: null,
      devTemplates : []
    };
  }
  setDevs(data){
    this.state = {
      developpers : data
    }
  }

  getDevs(){
    return this.state.developpers;
  }
  
  setDevDetails(data){
    this.state={
      details : data
    }
  }

  getDevDetails(){
    return this.state.details;
  }

  getDevsByPage(page){
    let cCount = [];
    if(this.state.developpers){
      var first = (page-1)*9;
      var last = (page*9)-1;
      
      var i;
      for (i = first; i <= last; i++) {
        if(this.state.developpers[i])
        cCount.push(this.state.developpers[i]);
      }
    }
    return cCount;

  }

  register (action) {
    switch(action.actionType) {
      
      case UserConstants.GET_DEVS:
        this.setDevs(action.userObject.content);
        this.emitChange();
      break;

      case UserConstants.GET_DEV_DETAILS:
        this.setDevDetails(action.userObject.content);
        this.emitChange();
      break;

    }
  }


}

export default new DevStore;