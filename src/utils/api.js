require('es6-promise').polyfill();
require('isomorphic-fetch');

import UserActions from '../actions/UserActions';
import UserConstants from '../constants/UserConstants';

class ApiHelper {

	constructor() {
		this.DEV_MODE = process.env.NODE_ENV === 'development';
	}

	// Helpers
	_consoleReqURL(url) {
		this.DEV_MODE && console.log('::REQUEST TO: ', url);
	}

	_consoleResDATA(data) {
		this.DEV_MODE && console.log('::REQUEST DATA: ', data);
	}

	_consoleReqERROR(error) {
		this.DEV_MODE && console.log('::REQUEST ERROR: ', error);
	}

	// GET
	get(url) {

		return fetch(url, {
			credentials: 'include'
		})
		.then(response => {
			this._consoleReqURL(url);
			return response.json();
		})
		.then(data => {
			this._consoleResDATA(data);

			if(data.data && data.data.error && !data.data.action) {
				console.log('::REQUEST ERROR (SESSIONS EXPIRED) --- Redirecting to the login page.');
				data.data.EXPIRED_SESSION = true;
				
				//UserActions.signTheUserOut();

				return data.data;
			}
		
			if(data.data && data.data.action) data.data.FINE = true
			else data.data.NOT_FINE = true;

			return data.data;
		})
		.catch(error => {
			this._consoleReqERROR(error);
			return {};
		});
	}

}

export default new ApiHelper;