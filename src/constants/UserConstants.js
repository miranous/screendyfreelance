/*
 * User Actions
 */

var keyMirror = require('keymirror');

var UserConstants = keyMirror({
	SIGN_IN: null,
	SIGN_IN_FAIL: null,
	IS_ACTIVE: null,
	IS_ACTIVE_POPUP: null,
	SIGN_OUT: null,
	DASHBOARD: null,
	ERROR: null,
	IS_FREELANCER: null,
	IS_COMPANY:null,
	GET_DEV_DETAILS: null,
	GET_DEV_APPS: null,
	ADD_DEV_APP: null,
	DEV_DATA: null,
	FOLLOW: null,
	DEV_TEMPLATES: null,
	GET_APP_DETAILS: null,
	LIKE: null,
	DISLIKE: null,
	SIGN_UP: null,
	SIGN_UP_FAIL : null,
	GET_ALL_JOBS : null,
	COMPANY_PROFIL : null
});

export default UserConstants;