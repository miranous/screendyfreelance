import React, { Component } from 'react';
import linkState from 'react-link-state';
import { Link } from 'react-router';
import DevStore from '../stores/DevStore';
import DevelopperActions from '../actions/DevelopperActions';
import Developper from '../developpers/developper';

export default class Home extends Component {

  constructor(props, context) {
    super(props, context);
    DevelopperActions.GetDeveloppersTop3();
    this.state = {
        SearchValue: '',
        devs : []
      }
    this.onChange=this.onChange.bind(this);
  }

  onChange(){
      this.setState({
        devs : DevStore.getDevs()
      });
  }

  componentWillMount(){
    DevStore.addChangeListener(this.onChange);
  }
  componentWillUnmount(){
    DevStore.removeChangeListener(this.onChange);
  }

  componentWillUpdate() {
    window.scrollTo(0, 0);
  }

  keydown(){
    this.refs.text.style.color="black";
    this.setState({SearchValue : ''});
  }
  Search(event){
    if(this.state.SearchValue){
      this.props.history.pushState(null, `Search/1/${this.state.SearchValue}`);
    }
    else
    {
      this.refs.text.style.color="red";
      this.refs.text.value="Missing keyword...";
    }
  }

  SearchEnter(event){
    if (event.key === 'Enter') {
      if(this.state.SearchValue){
        this.props.history.pushState(null, `Search/1/${this.state.SearchValue}`);
      }
      else
      {
        this.refs.text.style.color="red";
        this.refs.text.value="Missing keyword...";
      }
    }
  }

  devs(dev) {
      return(
        <div key={dev.idUser} className="col-md-4">
        <Developper developper={dev} />
        </div>
        );
    }

  render() {

    var tbody = this.state.devs.map(this.devs,this);
    var Search = this.Search.bind(this);
    var keydown = this.keydown.bind(this);
    var SearchEnter = this.SearchEnter.bind(this);

    return (
      <div className ="row">
      <div className="container">

        <h2 className="text-center">Get more done with <b>ScreenDy</b> Freelancers</h2>
        <br/>
          <div className="col-md-2"></div>
          <div className="col-md-8">
              <div className="input-group">

                  <input onKeyPress={SearchEnter} type="text" name="search" className="form-control input-search" ref="text" placeholder="Search..." onClick={keydown} valueLink={linkState(this, 'SearchValue')}/>
                  <span className="input-group-btn">
                        <button type="button" name="search" id="search-btn" className="btn btn-flat input-search" onClick={Search}><i className="fa fa-search"></i></button>
                  </span>
              </div>
              </div>
         
          <br/><br/><br/>
        <h3 className="text-center"> Work with someone perfect for your team</h3>
        <br/><br/>

          <div className="nav-tabs-custom">
                <ul className="nav nav-tabs pull-right">
                  <li className="pull-left header"> Featured Freelancers</li>
                </ul>
          </div>
       <div className ="row">
       {tbody}
       </div>
      </div>

      <br/><br/>


      <div className="row">
      <div className="col-md-5"/>
      <div className="col-md-2">
      <Link to='developpers/1/All'>
      <button type="button" className="btn btn-block btn-primary ">See More</button>
      </Link>
      </div>
      </div>

      <br/><br/>
      </div>
    );
  }

}
