import React, { Component } from 'react';
import { Link } from 'react-router';
import UserActions from '../actions/UserActions';
import UserStore from '../stores/UserStore';
import FreelancerStore from '../stores/FreelancerStore';
import CompanyStore from '../stores/CompanyStore';
export default class Header extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      Connected : false,
      Freelancer : false,
      Company : false,
      Review : false,
      fname : UserStore.getUser().firstName,
      lname : UserStore.getUser().lastName,
      email : UserStore.getUser().email,
      photo : UserStore.getUser().photo,
      id : UserStore.getUser().id
    };
    this._updateAuth = this._updateAuth.bind(this);
    this._updateFreelance = this._updateFreelance.bind(this);
    this.signout = this.signout.bind(this);
    this._updateCompany = this._updateCompany.bind(this);
  }

  signout() {
    UserActions.sign_out();
    this.props.changeRoute(null, '/');
  }

  componentWillMount() {
    UserStore.addChangeListener(this._updateAuth);
    FreelancerStore.addChangeListener(this._updateFreelance);
    CompanyStore.addChangeListener(this._updateCompany);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._updateAuth);
    FreelancerStore.removeChangeListener(this._updateFreelance);
    CompanyStore.removeChangeListener(this._updateCompany);
  }
  _updateCompany() {

  if (CompanyStore.isCompany()){
      this.setState({ 
        Company: true
      });
    if(CompanyStore.isReview()){
        this.setState({ 
          Review: true
        });
      }
    else{
          this.setState({ 
            Review: false,
            fname : CompanyStore.getCompany().namecompany,
            lname : '',
            email : CompanyStore.getCompany().email,
            photo : CompanyStore.getCompany().avatar
          });
        }
    }
    else{
      this.setState({ 
        Company : false
      });
    }

  }
  _updateFreelance() {

    if(FreelancerStore.isFreelancer()) {
      this.setState({ 
        Freelancer: true
      });
        if(FreelancerStore.isReview()) {
        this.setState({ 
          Review: true
        });
        }
        else{
          this.setState({ 
            Review: false,
            fname : FreelancerStore.getFreelancer().firstname,
            lname : FreelancerStore.getFreelancer().lastname,
            photo : FreelancerStore.getFreelancer().avatar
          });
        }
    }
    else{
      this.setState({ 
        Freelancer: false
      });
    }
  }

  _updateAuth() {
    if(UserStore.isSignedIn()) {
      this.setState({ 
        Connected: true,
        fname : UserStore.getUser().firstName,
        lname : UserStore.getUser().lastName,
        email : UserStore.getUser().email,
        photo : UserStore.getUser().photo,
        id : UserStore.getUser().id
      });
      UserActions.is_freelancer(UserStore.getUser().id);
      UserActions.is_company(UserStore.getUser().id);
    }
    else{
      this.setState({ 
        Connected: false
      });
    }

  }

  render() {

    if(this.state.Connected) {
      if(this.state.Freelancer) return this.renderFreelancer();
      else if (this.state.Company) return this.renderCompany();
      return this.renderSigned();
    } 

    return (
///////NOT LOGGED IN 
   <header className="main-header">
   
    <nav className="navbar navbar-fixed-top">

        <div className="navbar-header">
            <Link to="#"><img src="css/adminLTE/img/beta2.png" height="50"/><img src="css/adminLTE/img/logofreelancer.png" height="50"/></Link>
       
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i className="fa fa-bars"></i>
          </button>
        </div>

    
    <div id="navbar-collapse">
     <div className="navbar-collapse pull-left">
        <ul className="nav navbar-nav"> 
             <li><Link to="developpers/1/All"><b>Freelancers</b></Link></li>
             <li><Link to="jobs"><b>Jobs</b></Link></li>
         </ul>
      </div>

      <div className="navbar-form pull-right">
          <div>
              <span>
                <Link to='signup'>
                <button type="submit" name="search" id="search-btn" className="btn btn-primary"><i className="fa fa-fw fa-pencil-square-o"></i>SIGN UP</button>
                </Link>
              </span>
              &nbsp;&nbsp;&nbsp;
              <span>
              <Link to='signin'>
              <button type="submit" name="search" id="search-btn" className="btn btn-primary"><i className="fa fa-fw fa-sign-in"></i>LOGIN</button>
              </Link>
                &nbsp;&nbsp;&nbsp;
              <Link to='signin' className="btn btn-default ">
                  <span>Become a Freelancer</span>
              </Link>
               &nbsp;&nbsp;&nbsp;
              <Link to='signin' className="btn btn-primary ">
                  <span>Post a job</span>
              </Link>
              </span>
          </div>
      </div>
     </div>
      
  </nav>
  <br/><br/>
  </header>
    );
  }

  renderSigned() {
    return (
/////// ONLY SIGNED IN
   <header className="main-header">
   
    <nav className="navbar navbar-fixed-top">
        <div className="navbar-header">
            <Link to="#"><img src="css/adminLTE/img/beta2.png" height="50"/><img src="css/adminLTE/img/logofreelancer.png" height="50"/></Link>
       
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i className="fa fa-bars"></i>
          </button>
        </div>

    <div id="navbar-collapse">
     <div className="navbar-collapse pull-left">
        <ul className="nav navbar-nav"> 
             <li><Link to="developpers/1/All"><b>Freelancers</b></Link></li>
             <li><Link to="jobs"><b>Jobs</b></Link></li>
         </ul>
      </div>
      <div className="navbar-custom-menu">
      <ul className="nav navbar-nav">
      <li  className="dropdown user user-menu">
    
          <a href="#" className="dropdown-toggle" data-toggle="dropdown">

            <img src={this.state.photo} className="user-image" alt="User Image" />
              <span className="hidden-xs">{this.state.fname}&nbsp;{this.state.lname}</span>
          </a>
          <ul className="dropdown-menu">

            <li className="user-header">
               <img src={this.state.photo} className="img-circle" alt="User Image" />
               <p>
                  {this.state.fname}&nbsp;{this.state.lname}
                  <small>{this.state.email}</small>
                </p>
            </li>
            <li className="user-footer">
                    <div className="pull-right">
                      <button type="button" onClick={this.signout} className="btn btn-default btn-flat">Sign out</button>
                    </div>
            </li>
            
          </ul>
          
          </li> 
         
        </ul>
        <div className="navbar-form pull-right">
              <Link to='Choose' className="btn btn-default ">
              <span>Get Started</span>
              </Link>
        </div>
      </div>
      
     </div>
      
  </nav>
  <br/><br/>
  </header>
    );
  }

  renderFreelancer() {
//// HEADER FREELANCER
    var review = null;
    if(this.state.Review) 
      review = <div className="pull-left">
                      <img src="http://ec.europa.eu/echo/files/civil_protection/vademecum/images/Under_Review_Stamp.png" height="30" width="100" />
                    </div>;
    else
      review = <div className="pull-left">
                      <Link to={`profile/${this.state.id}`} type="button" className="btn btn-default btn-flat">Update Profile</Link>
                    </div>


    return (

   <header className="main-header">
   
    <nav className="navbar navbar-fixed-top">
   
    <div className="navbar-header">
            <Link to="#"><img src="css/adminLTE/img/beta2.png" height="50"/><img src="css/adminLTE/img/logofreelancer.png" height="50"/></Link>
       
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i className="fa fa-bars"></i>
          </button>
    </div>
    <div className="navbar-collapse pull-left">
        <ul className="nav navbar-nav"> 
             <li><Link to="developpers/1/All"><b>Freelancers</b></Link></li>
             <li><Link to="jobs"><b>Jobs</b></Link></li>
         </ul>
    </div>
    <div className="navbar-custom-menu">
      <ul className="nav navbar-nav">
      <li  className="dropdown user user-menu">
    
          <a href="#" className="dropdown-toggle" data-toggle="dropdown">

            <img src={this.state.photo} className="user-image" alt="User Image" />
              <span className="hidden-xs">{this.state.fname}&nbsp;{this.state.lname}</span>
          </a>
          <ul className="dropdown-menu">

            <li className="user-header">
               <img src={this.state.photo} className="img-circle" alt="User Image" />
               <p>
                  {this.state.fname}&nbsp;{this.state.lname}
                  <small>{this.state.email}</small>
                </p>
            </li>
            <li className="user-footer">
                    {review}
                    <div className="pull-right">
                      <button type="button" onClick={this.signout} className="btn btn-default btn-flat">Sign out</button>
                    </div>
            </li>
          </ul>
          </li> 
        </ul>
      </div>
  </nav>
  <br/><br/>
  </header>
    );
  }

  renderCompany() {
//// HEADER COMPANY
    var review = null;
    if(this.state.Review) 
      review = <div className="pull-left">
                      <img src="http://ec.europa.eu/echo/files/civil_protection/vademecum/images/Under_Review_Stamp.png" height="30" width="100" />
                    </div>;
    else
      review = <div className="pull-left">
                      <Link to={`profilecompany/${this.state.id}`} type="button" className="btn btn-default btn-flat">Update Profile</Link>
                    </div>


    return (

   <header className="main-header">
   
    <nav className="navbar navbar-fixed-top">
   
    <div className="navbar-header">
            <Link to="#"><img src="css/adminLTE/img/beta2.png" height="50"/><img src="css/adminLTE/img/logofreelancer.png" height="50"/></Link>
       
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i className="fa fa-bars"></i>
          </button>
    </div>
    <div className="navbar-collapse pull-left">
        <ul className="nav navbar-nav"> 
             <li><Link to="developpers/1/All"><b>Freelancers</b></Link></li>
             <li><Link to="jobs"><b>Jobs</b></Link></li>
         </ul>
    </div>
    <div className="navbar-custom-menu">
      <ul className="nav navbar-nav">
      <li  className="dropdown user user-menu">
    
          <a href="#" className="dropdown-toggle" data-toggle="dropdown">

            <img src={this.state.photo} className="user-image" alt="User Image" />
              <span className="hidden-xs">{this.state.fname}&nbsp;{this.state.lname}</span>
          </a>
          <ul className="dropdown-menu">

            <li className="user-header">
               <img src={this.state.photo} className="img-circle" alt="User Image" />
               <p>
                  {this.state.fname}&nbsp;{this.state.lname}
                  <small>{this.state.email}</small>
                </p>
            </li>
            <li className="user-footer">
                    {review}
                    <div className="pull-right">
                      <button type="button" onClick={this.signout} className="btn btn-default btn-flat">Sign out</button>
                    </div>
            </li>
          </ul>
          </li> 
        </ul>
      </div>
  </nav>
  <br/><br/>
  </header>
    );
  }

}

