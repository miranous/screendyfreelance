import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';

export default class SecondHeader extends React.Component{

   constructor(props, context) {
    super(props, context);

    }


  render() {
  
    return (
   <header >
    <div className="navbar navbar-default topmargin" >
      <div className="container ">
        
      <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed btn-default" data-toggle="collapse" data-target="#navbar-collapse-2">
            <i className="fa fa-bars "></i>
          </button>
      </div>
      <div className="pull-right" id="navbar-collapse-2">
          <ul className="nav navbar-nav bold">
           <li className="nav-item">
            <a className="nav-link" href="http://web.screendy.com/" target="_blank"><b>ScreenDy</b></a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://showcase.screendy.com" target="_blank"><b>Showcase</b></a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://platform.screendy.com" target="_blank"><b>Platform</b></a>
          </li>
         <li className="nav-item">
            <a className="nav-link" href="http://docs.screendy.com" target="_blank"><b>Docs</b></a>
          </li> 
          <li className="nav-item">
            <a className="nav-link" href="http://playground.screendy.com/" target="_blank"><b>Playground</b></a>
          </li>
          
          <li className="nav-item">
            <a className="nav-link" href="http://market.screendy.com/" target="_blank"><b>Market</b></a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://freelancers.screendy.com/" target="_blank"><b>Freelancers</b></a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://www.hackandpitch.com" target="_blank"><b>Hack & Pitch</b></a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://support.screendy.com" target="_blank"><b>Support</b></a>
          </li>
         </ul>
      </div>
      </div>

  </div>
 </header>


    );
  }
}