import React, { Component } from 'react';
import { Link } from 'react-router';
import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';

class Signin extends Component {


  constructor(props, context) {

    super(props, context);

    this.state = {
      isAuthenticating: false,
      authError: false
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this._updateAuth = this._updateAuth.bind(this);
  }

  _updateAuth() {
    if(UserStore.isSignedIn()) {
          this.props.history.pushState(null, `/`);
    }
    this.setState({
        isAuthenticating: false 
      });
    if(!UserStore.isSignedIn()) {
      this.setState({ 
        authError: true
      });
    }

  }

  componentWillUpdate () {
    window.scrollTo(0, 0)
  }

  componentWillMount() {
    UserStore.addChangeListener(this._updateAuth);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._updateAuth);
  }


  handleSubmit(event) {
    event.preventDefault();

    const email    = this.refs.email.value;
    const password = this.refs.password.value;

    this.setState({ isAuthenticating: true });

    UserActions.signTheUserIn({email,password});
  
  }

  render() {

    if(this.state.isAuthenticating) return this._renderHTMLLoading();
      
    return this._renderHTML();

  }

  _renderHTML() {
    return (
  <div className="login-box">
  <div className="login-logo">
    <img src="http://web.screendy.com/wp-content/uploads/2015/05/logoScreendy.png" height="50"/>
  </div>
    <div className="login-box-body">
    {this.state.authError ? 
            <p className="login-box-msg" style={{color: 'red'}}><b>Wrong Email or Password !</b></p> 
            : 
            <p className="login-box-msg">Sign in using your <b>ScreenDy</b> account to start your session</p>
          } 
    
      <form onSubmit={this.handleSubmit}>
      <div className="form-group has-feedback">
        <input ref="email" type="email" className="form-control" placeholder="Email" />
        <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input ref="password" type="password" className="form-control" placeholder="Password"/>
        <span className="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div className="row">
        <div className="col-xs-8">
          
        </div>
        <div className="col-xs-4">
          <button type="submit" className="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
    <a href="http://screendydev.cloudapp.net/screendy/front/studio/v097917/?msg=#" target="_blank">I forgot my password</a><br/>
    <Link to='/signup' className="text-center">Register a new membership</Link>
      </div>
      </div>
    );
  }

  _renderHTMLLoading() {
    return(
    <div className="login-box">
      <div className="login-logo">
        <img src="http://web.screendy.com/wp-content/uploads/2015/05/logoScreendy.png" height="50"/>
      </div>
      <div className="box">
          {this.state.authError ? 
                      <p className="login-box-msg" style={{color: 'red'}}>{this.state.authMessage}</p> 
                      : 
                      <p className="login-box-msg">Sign in to your <b>ScreenDy</b> account</p>
                    } 
    
            <form onSubmit={this.handleSubmit}>
            <div className="form-group has-feedback">
              <input ref="email" type="email" className="form-control" placeholder="Email" />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input ref="password" type="password" className="form-control" placeholder="Password"/>
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div className="row">
              <div className="col-xs-8">
                
              </div>
              <div className="col-xs-4">
                <button type="submit" className="btn btn-primary btn-block btn-flat">Sign In</button>
              </div>
            </div>
              </form>
          <a href="http://screendydev.cloudapp.net/screendy/front/studio/v097917/?msg=#" target="_blank">I forgot my password</a><br/>
          <Link to='/signup' className="text-center">Register a new membership</Link>
             <div className="overlay">
                <i className="fa fa-refresh fa-spin"></i>
              </div>
        </div>
      </div>

      );
  }

}

Signin.contextTypes = {
  history: React.PropTypes.object
};

export default Signin
