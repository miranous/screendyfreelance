import React, { Component } from 'react';
export default class ContentContainer extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div >
        <section>
          {this.props.children}
        </section>
      </div>
    );
  }

}

