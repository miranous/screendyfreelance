import React, { Component } from 'react';
import { Link } from 'react-router';
import UserStore from '../stores/UserStore';
export default class Choose extends Component {

  constructor(props, context) {
    super(props, context);
    if(!UserStore.isSignedIn())
    {
      this.props.history.pushState(null, `signin`);
    }
  }

  render() {
    return (
       <div className="container  text-center">
              
       <section className="content-header">
              <ol className="breadcrumb">
                <li><Link to='Home'><i className="fa fa-dashboard"></i>Home</Link></li>
                <li className="active">Details</li>
              </ol>
        </section>
        <h2 className="row">
                 Complete your profile
        </h2>
        <br/><br/><br/>
        <section className="content-body">
        <div className="row">
            <div className="col-md-1"/>
            <div className="col-md-5" style={{"background-color":"white","border":"2px groove"}}>
              <h1 className="glyphicon glyphicon-user"></h1>
              <h3>I want to <strong>become</strong> a ScreenDy freelancer</h3>
              <h5>Find freelance projects and grow your business.</h5>
              <br/>
              <p className="col-md-4"/>
              <Link to='becomedev'  className="btn btn-primary col-md-4">Work</Link>
              <br/><br/><br/><br/>
            </div>
            <div className="col-md-1"/>
            <div className="col-md-5" style={{"background-color":"white","border":"2px groove"}}>
              <h1 className="glyphicon glyphicon-home"></h1>
              <h3>I want to <strong>hire</strong> a ScreenDy freelancer</h3>
              <h5>Find, collaborate with, and pay an expert.</h5>
              <br/>
              <p className="col-md-4"/>
              <Link to='hiredev'  className="btn btn-primary col-md-4">Hire</Link>
              <br/><br/><br/><br/>
            </div>
        </div>
        </section>
        <br/><br/><br/><br/>
      </div>
    );
  }

}