import React, { Component } from 'react';
import { Link } from 'react-router';
import UserStore from '../stores/UserStore';
import { Modal } from 'react-bootstrap';
export default class Hire extends Component {

  constructor(props, context) {
    super(props, context);
    if(!UserStore.isSignedIn())
    {
      this.props.history.pushState(null, `signin`);
    }
    this.state = {
          showmodal: false,
          show : false,
          errors : {},
          showresult : false
        }
    this.create = this.create.bind(this);
    this.show=this.show.bind(this);
    this.ComponentFormIsValid = this.ComponentFormIsValid.bind(this);
    this.YesConfirmation = this.YesConfirmation.bind(this);
    this.showresultClose=this.showresultClose.bind(this);
  }
show(){
  this.setState({show : true});  
 }
showresultClose(){
    this.setState({
      showresult : false
    });
  }
YesConfirmation(){
  this.ComponentFormIsValid();
 }
create() {
      if (!this.ComponentFormIsValid()) {
        event.preventDefault();

        return;
      }
      else{

      var form_data = new FormData();
      var that = this;
      form_data.append('idUser', UserStore.getUser().id);
      form_data.append('namecompany', this.refs.company.value);
      form_data.append('avatar', this.refs.logo.files[0]);
      form_data.append('emailcompany', this.refs.email.value);
      form_data.append('locationcompany', this.refs.location.value);
      form_data.append('descriptioncompany', this.refs.description.value);
      $.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/createCompany',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){
                                that.setState({ 
                                showresult : true
                                 });  
                                
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                
                              }
                   });
      this.props.history.pushState(null, `/`);
    }
      
    }
  ComponentFormIsValid() {
        var formIsValid = true;
        this.state.errors = {};

        if (! this.refs.company.value || this.refs.company.length < 1) {
        this.state.errors.company = 'Please fill in your Company name';
        formIsValid = false;
      }

     if (! this.refs.logo.files[0]) {
        this.state.errors.logo = 'Please fill in your Company logo';
        formIsValid = false;
      }

      if (! this.refs.email.value || this.refs.email.length < 1) {
        this.state.errors.email = 'Please fill in your email.';
        formIsValid = false;
      }
      if (! this.refs.location.value || this.refs.location.length < 1) {
        this.state.errors.location = 'Please fill in your location';
        formIsValid = false;
      }

      if (! this.refs.description.value || this.refs.description.length < 1) {
        this.state.errors.description = 'Please fill in the company description';
        formIsValid = false;
      }

      /*if (! this.refs.avatar.files[0] || (this.refs.avatar.value.match(/\.(.+)$/)[0] != '.PNG' && this.refs.avatar.value.match(/\.(.+)$/)[0] != '.png' && this.refs.avatar.value.match(/\.(.+)$/)[0] != '.jpg' && this.refs.avatar.value.match(/\.(.+)$/)[0] != '.JPG')) {
        this.state.errors.avatar = 'You must use a PNG or JPG Avatar.';
        formIsValid = false;
      }*/
        this.setState({errors: this.state.errors});
        return formIsValid;

      }
  render() {
        var wrapperClassCompany = 'form-group';
          if (this.state.errors.company && this.state.errors.company.length > 0) {
            wrapperClassCompany += " " + 'has-error';
          }
        var wrapperClassEmail = 'form-group';
          if (this.state.errors.email && this.state.errors.email.length > 0) {
            wrapperClassEmail += " " + 'has-error';
          }
        var wrapperClassLogo = 'form-group';
          if (this.state.errors.logo) {
            wrapperClassLogo += " " + 'has-error';
          }

        var wrapperClassLocation = 'form-group';
          if (this.state.errors.location && this.state.errors.location.length > 0) {
            wrapperClassLocation += " " + 'has-error';
          }
        var wrapperClassDescription = 'form-group';
          if (this.state.errors.description && this.state.errors.description.length > 0) {
            wrapperClassDescription += " " + 'has-error';
          }
    return (
       <div>
        <div>
        <h2 className="text-center">Create a Company Account</h2>
        </div>
        <br />
        <div className="row">
        <div className="col-md-8 col-md-offset-2">
          <div className="box box-solid">
            <div className="box-header with-border">
              <i className="glyphicon glyphicon-question-sign"></i>

              <h3 className="box-title">How it works</h3>
            </div>
            <div className="box-body">
              <blockquote>
                <p><b>ScreenDy</b> Freelancers is a great place to find freelancers and grow your own business.</p>
                <p>Here you have :</p>
                <ul>
                <li>The freedom to add any job you want to display for free</li>
                <li>The chance to choose your own freelancers</li>
                <li>The opportunity to find developers to work with</li>
                </ul>
             <p>Just complete your profile and find out !</p>
              </blockquote>
              <div className="col-md-6 col-md-offset-5">
              <button className="btn btn-success " onClick={this.show}>Ready to hire ?</button>
              </div>
            </div>
          </div>
        </div>
        </div>
        {this.state.show ?

        <div className="row">
         {window.scrollTo(0, 580)}
        <div className="col-md-8 col-md-offset-2">
          <div className="box box-default">
            <div className="box-body">

                <div className={wrapperClassCompany}>
                  Company's name *
                  <input type="text" className="form-control" ref="company"/>
                  <div className="input text-red">{this.state.errors.company}</div> 
                </div>

                <div className={wrapperClassLogo}>
                Company's logo *
                  <input type="file" className="form-control" ref="logo"></input>
                  <div className="input text-red">{this.state.errors.logo}</div>
                </div>
                <div className={wrapperClassEmail}>
                Email *
                  <input type="text" className="form-control" ref="email"></input>
                  <div className="input text-red">{this.state.errors.email}</div>
                </div>
                <div className={wrapperClassLocation}>
                Company's location *
                  <input type="text" className="form-control" ref="location"/>
                  <div className="input text-red">{this.state.errors.location}</div>
                </div>
                <div className={wrapperClassDescription}>
                Company's description *
                  <input type="text" className="form-control" ref="description"/>
                  <div className="input text-red">{this.state.errors.description}</div>
                </div>
                <div className="col-md-6 col-md-offset-5">
                <button  className="btn btn-success" onClick={this.create}>Get Started</button>
                </div>

                </div>
                </div>

            </div>
          
          
          <br /><br /><br />
               </div>
        : 
        <div/>}


        <Modal show={this.state.showresult} onHide={this.showresultClose}>
                <Modal.Body>
                 <i className="fa fa-check "></i> Your profile was successfully updated 
                </Modal.Body>
        </Modal>
      
      </div>
    );
  }

}