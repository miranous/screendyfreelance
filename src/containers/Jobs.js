import React, { Component } from 'react';
import linkState from 'react-link-state';
import { Link } from 'react-router';
import JobList from '../jobs/joblist';

export default class Jobs extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
        SearchValue: ''
      }
  }

  keydown(){
    this.refs.text.style.color="black";
    this.setState({SearchValue : ''});
  }

  Search(event){
    if(this.state.SearchValue){
      this.props.history.pushState(null, `Search/1/${this.state.SearchValue}`);
    }
    else
    {
      this.refs.text.style.color="red";
      this.refs.text.value="Missing keyword...";
    }
  }

  SearchEnter(event){
    if (event.key === 'Enter') {
      if(this.state.SearchValue){
        this.props.history.pushState(null, `Search/1/${this.state.SearchValue}`);
      }
      else
      {
        this.refs.text.style.color="red";
        this.refs.text.value="Missing keyword...";
      }
    }
  }

  componentWillUpdate () {
    window.scrollTo(0, 0)
  }

  render() {
    var Search = this.Search.bind(this);
    var keydown = this.keydown.bind(this);
    var SearchEnter = this.SearchEnter.bind(this);
    return (
     <div className ="row">
      <div className="container">
        <section className="content-header">
              <ol className="breadcrumb">
                <li><Link to='home'><i className="fa fa-dashboard"></i>Home</Link></li>
                <li className="active">Top Navigation</li>
              </ol>
          </section>

          <h2 className="text-center">Browse our greatest Jobs</h2>
          <br/>
          <div className="col-md-2"></div>
                <div className="col-md-8">
                   <div className="input-group">
                             <input onKeyPress={SearchEnter} type="text" name="search" className="form-control input-search" ref="text" placeholder="Search..." onClick={keydown} valueLink={linkState(this, 'SearchValue')}/>
                             <span className="input-group-btn">
                               <button type="button" name="search" id="search-btn" className="btn btn-flat input-search" onClick={Search}><i className="fa fa-search"></i></button>
                            </span>
                    </div>
                </div>
         
          <br/><br/><br/><br/><br/>

          <JobList />
        
        </div>
      </div>
    );
  }

}