import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';

export default class Footer extends React.Component{

  render() {
    return (
      <div >
  <footer className="foortertable row">
  
  <div className="col-sm-2">
      <div>
    <section id="block-block-1" className="block block-block clearfix">
    </section>
  </div>
  </div>

  <div className="col-sm-2">
    <div>
    <section id="block-menu-menu-site-plan" className="block block-menu clearfix">
      <ul className="menu nav">
        <li className="firstleaf"><a>Product</a></li>
        <li className="leaf"><a href="http://web.screendy.com/" target="_blank">ScreenDy</a></li>
        <li className="leaf"><a href="http://playground.screendy.com/" target="_blank">Playground</a></li>
        <li className="leaf"><a href="http://platform.screendy.com/" target="_blank">Platform</a></li>
        <li className="leaf"><a href="http://screendymarket.cloudapp.net:8080/MarketplaceSites/contributor" target="_blank">Developpers</a></li>
        
      </ul>
    </section>
  </div>
  </div>

  <div className="col-sm-2">
    <div>
    <section id="block-menu-menu-site-plan-2" className="block block-menu clearfix">
      <ul className="menu nav">
        <li className="firstleaf"><a>Documentation</a></li>
        <li className="leaf"><a href="http://screendydev.cloudapp.net/screendy/front/doc2/v13/#/Tutorial/lessons" target="_blank">Vidéo tutorials</a></li>
        <li className="leaf"><a href="http://screendydev.cloudapp.net/screendy/front/doc2/v13/#/Developers/Welcome" target="_blank">Documentation</a></li>
        <li className="leaf"><a href="http://forum.screendy.com/" target="_blank">Forum</a></li>
        <li className="leaf"><a href="http://screendydev.cloudapp.net/screendy/front/doc2/v13/#" target="_blank">Tutorials</a></li>
      </ul>
    </section>
    </div>
  </div>

  <div className="col-sm-2">
    <div>
      <section id="block-menu-menu-site-plan-3" className="block block-menu clearfix">
        <ul className="menu nav">
          <li className="firstleaf"><a>About Us</a></li>
          <li className="leaf"><a href="http://web.screendy.com/blog/" target="_blank">Blog</a></li>
          <li className="leaf"><a href="http://screendydev.cloudapp.net/screendy/front/doc2/v13/#/Tutorial/media" target="_blank">Media</a></li>
          <li className="leaf"><a href="http://web.screendy.com/team/" target="_blank">Team</a></li>
          <li className="leaf"><a href="http://screendydev.cloudapp.net/screendy/front/doc2/v13/#/Tutorial/faq" target="_blank">FAQ</a></li>
      </ul>
      </section>
    </div>
  </div>
  <div className="col-sm-2">
  

    <div>
      <section id="block-menu-menu-site-plan-3" className="block block-menu clearfix">
      <ul className="menu nav">
      <li>
      <div className="firstleaf row ">
                <p className="col-sm-2"><a href="http://www.facebook.com/Screendy" target="_blank"><span className="btn btn-social-icon btn-facebook"><i className="fa fa-facebook"></i></span></a></p>
                <p className="col-sm-2"><a href="http://twitter.com/screendy" target="_blank"><span className="btn btn-social-icon btn-twitter"><i className="fa fa-twitter"></i></span></a></p>
                <p className="col-sm-2"><a href="http://www.linkedin.com/company/screendy" target="_blank"><span className="btn btn-social-icon btn-linkedin"><i className="fa fa-linkedin"></i></span></a></p> 
                <p className="col-sm-2"><a href="http://www.youtube.com/screendy" target="_blank"><span className="btn btn-social-icon btn-google"><i className="fa fa-youtube"></i></span></a></p>
          </div>
        <br/>
      </li>
      <li>
        <table className='bgtable'>
           <tbody>
              <tr>
              <td><i className="glyphicon glyphicon-home gliphfooter"></i></td>
              <td>&nbsp;1355 Market St. Suite #488 San Francisco, CA</td>
              </tr>
              <tr>
              <td><i className="glyphicon glyphicon-envelope gliphfooter"></i></td>
              <td>&nbsp;hello@screendy.com</td>
              </tr>
              <tr>
              <td><i className="glyphicon glyphicon-earphone gliphfooter"></i></td>
              <td>&nbsp;+1 415-906-5148</td>
              </tr>
            </tbody>
        </table>
      </li>
          
      </ul>
      </section>
    </div>
  </div>
  <br/>

  <div className="col-sm-12 text-center ">
  <br/><br/>
 <strong>Copyright &copy; 2016 <a href="http://web.screendy.com/" target="_blank">ScreenDy</a>.</strong> All rights reserved.
  </div>

  </footer>
  
  </div>
  
          
    );
  }
}