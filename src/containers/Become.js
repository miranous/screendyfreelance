import React, { Component } from 'react';
import UserStore from '../stores/UserStore';
import { Modal } from 'react-bootstrap';
import UserActions from '../actions/UserActions';
import TagsInput from 'react-tagsinput';

export default class Become extends Component {

	constructor(props, context) {
    	super(props, context);

    	this.state = {
		    	errors: {},
		    	skills:[],
		    	showmodal: false,
		    	show : false
	    	}
    	this.create = this.create.bind(this);
    	this.ComponentFormIsValid = this.ComponentFormIsValid.bind(this);
    	this.YesConfirmation = this.YesConfirmation.bind(this);
    	this.handleSkillsChange = this.handleSkillsChange.bind(this);
    	this.show=this.show.bind(this);
  	}

  	handleSkillsChange(skills) {
        this.setState({skills})
      }

    componentDidMount () {
	    window.scrollTo(0, 0);
	    
	  }

  	ComponentFormIsValid() {
    		var formIsValid = true;
    		this.state.errors = {};

    		if (! this.refs.firstname.value || this.refs.firstname.length < 1) {
				this.state.errors.firstname = 'Please fill your First Name.';
				formIsValid = false;
			}

			if (! this.refs.lastname.value || this.refs.lastname.length < 1) {
				this.state.errors.lastname = 'Please fill your Last Name.';
				formIsValid = false;
			}

			if (! this.refs.email.value || this.refs.email.length < 1) {
				this.state.errors.email = 'Please fill your email.';
				formIsValid = false;
			}

			if (! this.refs.email.value || this.refs.email.length < 1) {
				this.state.errors.email = 'Please fill your email.';
				formIsValid = false;
			}

			if (! this.refs.description.value || this.refs.description.length < 1) {
				this.state.errors.description = 'Please fill your job title.';
				formIsValid = false;
			}

			if (! this.refs.avatar.files[0] || (this.refs.avatar.value.match(/\.(.+)$/)[0] != '.PNG' && this.refs.avatar.value.match(/\.(.+)$/)[0] != '.png' && this.refs.avatar.value.match(/\.(.+)$/)[0] != '.jpg' && this.refs.avatar.value.match(/\.(.+)$/)[0] != '.JPG')) {
				this.state.errors.avatar = 'You must use a PNG or JPG Avatar.';
				formIsValid = false;
			}



    		this.setState({errors: this.state.errors});
    		return formIsValid;

    	}

    YesConfirmation() {
    	UserActions.is_freelancer(UserStore.getUser().id);
    	UserActions.is_freelancer(UserStore.getUser().id);
    	UserActions.is_freelancer(UserStore.getUser().id);
    	this.props.history.pushState(null, `/`);
    }

  	create() {
  		if (!this.ComponentFormIsValid()) {
				event.preventDefault();

				return;
			}
			else{

  		var form_data = new FormData();
  		form_data.append('idUser', UserStore.getUser().id);
  		form_data.append('firstname', this.refs.firstname.value);
  		form_data.append('lastname', this.refs.lastname.value);
  		form_data.append('email', this.refs.email.value);
  		form_data.append('country', this.refs.country.value);
  		form_data.append('city', this.refs.city.value);
  		form_data.append('description', this.refs.description.value);
  		form_data.append('overview', "");
  		form_data.append('skills', JSON.stringify(this.state.skills));
  		form_data.append('facebook', this.refs.facebook.value);
  		form_data.append('linkedin', this.refs.linkedin.value);
  		form_data.append('git', this.refs.git.value);
  		form_data.append('avatar', this.refs.avatar.files[0]);
  		$.ajax({
                              url: 'http://screendymarket.cloudapp.net:8080/Marketplace/createFreelance',
                              contentType: false,
                              processData: false,
                              cache: false,
                              data: form_data,                   
                              type: 'POST',
                              success: function(response){
                                  
                              },
                              error: function (xhr, ajaxOptions, thrownError) {
                                
                              }
                   });
  		this.setState({ 
							        showmodal: true
							      });
  	}
  		
  	}
  	show(){
  		this.setState({show : true});
  		
  	}
  	render() {

  			var wrapperClassFirstname = 'form-group';
		      if (this.state.errors.firstname && this.state.errors.firstname.length > 0) {
		        wrapperClassFirstname += " " + 'has-error';
		      }
		    var wrapperClassLastname = 'form-group';
		      if (this.state.errors.lastname && this.state.errors.lastname.length > 0) {
		        wrapperClassLastname += " " + 'has-error';
		      }
		    var wrapperClassEmail = 'form-group';
		      if (this.state.errors.email && this.state.errors.email.length > 0) {
		        wrapperClassEmail += " " + 'has-error';
		      }
		    var wrapperClassDescription = 'form-group';
		      if (this.state.errors.description && this.state.errors.description.length > 0) {
		        wrapperClassDescription += " " + 'has-error';
		      }
		    var wrapperClassOverview = 'form-group';
		      if (this.state.errors.overview && this.state.errors.overview.length > 0) {
		        wrapperClassOverview += " " + 'has-error';
		      }
		    var wrapperClassAvatar = 'form-group';
		      if (this.state.errors.avatar && this.state.errors.avatar.length > 0) {
		        wrapperClassAvatar += " " + 'has-error';
		      }

    return (
      <div>
      	<div>
        <h2 className="text-center">Create a Freelancer Account</h2>
      	</div>
      	<br />
      	<div className="row">
        <div className="col-md-8 col-md-offset-2">
          <div className="box box-solid">
            <div className="box-header with-border">
              <i className="glyphicon glyphicon-question-sign"></i>

              <h3 className="box-title">How it works</h3>
            </div>
            <div className="box-body">
              <blockquote>
                <p><b>ScreenDy</b> Freelancers is a great place to find clients and grow your own freelance business.</p>
                <p>Here you have :</p>
                <ul>
                <li>The freedom to add any type of projects you want to display</li>
                <li>The chance to choose your own clients and projects</li>
                <li>The opportunity to find developers to work with</li>
                <li>A place where you can be acknowledged for your work </li>
                <li>A higher rank each time you add more creations</li>
                </ul>
                <p>The greater likes you have on projects and follows you get, the more likely you are to get hired by clients that use ScreenDy</p>
                <p>Just complete your profile and find out !</p>
              </blockquote>
              <div className="col-md-6 col-md-offset-5">
              <button className="btn btn-success " onClick={this.show}>Ready to get hired?</button>
              </div>
            </div>
          </div>
        </div>
        </div>
        {this.state.show ?

      	<div className="row">
      	 {window.scrollTo(0, 580)}
      	<div className="col-md-8 col-md-offset-2">
      		<div className="box box-default">
      			<div className="box-body">

      				<div className="form-group">
						<div>
						First name *
							<div className={wrapperClassFirstname}>
								<input type="text"
										name="firstname"
										className="form-control"
										placeholder="First Name"
										ref="firstname"/>
								<div className="input text-red">{this.state.errors.firstname}</div>
							</div>
						</div>
					</div>

					<div className="form-group">
						<div>
							Last name *
							<div className={wrapperClassLastname}>
								<input type="text"
										name="lastname"
										className="form-control"
										placeholder="Last Name"
										ref="lastname"/>
								<div className="input text-red">{this.state.errors.lastname}</div>
							</div>
						</div>
					</div>

					<div className="form-group">
						<div>
							Professional email *
							<div className={wrapperClassEmail}>
								<input type="text"
										name="email"
										className="form-control"
										placeholder="Email"
										ref="email"/>
								<div className="input text-red">{this.state.errors.email}</div>
							</div>
						</div>
					</div>

					

					<div className="form-group">
						<div>
							<div className={wrapperClassAvatar}>
								Profil picture *
								<input type="file"
									name="avatar"
									className="form-control"
									placeholder="avatar"
									ref="avatar"/>
								<div className="input text-red">{this.state.errors.avatar}</div>
							</div>
						</div>
					</div>

					<div className="form-group">
						<div>
							<div className={wrapperClassDescription}>
							Job *
								<select
									className="form-control"
									id="job" 
									name="job"
									ref="job"
									ref="description">
										<option value="">Choose a job</option>
										<option value="Frontend developer">Frontend developer</option>
										<option value="Backend developer">Backend developer</option>
										<option value="Mobile developer">Mobile developer</option>
										<option value="ScreenDy developer">ScreenDy developer</option>
										<option value="UI Designer">UI Designer</option>
								</select>
								<div className="input text-red">{this.state.errors.description}</div>
							</div>
						</div>
					</div>

					<div className="form-group">
						<div>
							<div className='form-group'>
							<p>A skill can be a programming language, a technologie or platform you use</p>
							<p>Press the button "Enter" or "Tab" to add another skill </p>
								<TagsInput inputProps={{className: 'react-tagsinput-input',placeholder: 'Skills'}} value={this.state.skills} onChange={this.handleSkillsChange} />
								<div className="input text-red"></div>
							</div>
						</div>
					</div>
					<div className="form-group">
						<div>
							<div className='form-group'>
							Country
								<select
									className="form-control"
									id="country" 
									name="country"
									ref="country">
										<option value="">Choose your country</option>
										<option value="Afganistan">Afghanistan</option>
										<option value="Albania">Albania</option>
										<option value="Algeria">Algeria</option>
										<option value="American Samoa">American Samoa</option>
										<option value="Andorra">Andorra</option>
										<option value="Angola">Angola</option>
										<option value="Anguilla">Anguilla</option>
										<option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
										<option value="Argentina">Argentina</option>
										<option value="Armenia">Armenia</option>
										<option value="Aruba">Aruba</option>
										<option value="Australia">Australia</option>
										<option value="Austria">Austria</option>
										<option value="Azerbaijan">Azerbaijan</option>
										<option value="Bahamas">Bahamas</option>
										<option value="Bahrain">Bahrain</option>
										<option value="Bangladesh">Bangladesh</option>
										<option value="Barbados">Barbados</option>
										<option value="Belarus">Belarus</option>
										<option value="Belgium">Belgium</option>
										<option value="Belize">Belize</option>
										<option value="Benin">Benin</option>
										<option value="Bermuda">Bermuda</option>
										<option value="Bhutan">Bhutan</option>
										<option value="Bolivia">Bolivia</option>
										<option value="Bonaire">Bonaire</option>
										<option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
										<option value="Botswana">Botswana</option>
										<option value="Brazil">Brazil</option>
										<option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
										<option value="Brunei">Brunei</option>
										<option value="Bulgaria">Bulgaria</option>
										<option value="Burkina Faso">Burkina Faso</option>
										<option value="Burundi">Burundi</option>
										<option value="Cambodia">Cambodia</option>
										<option value="Cameroon">Cameroon</option>
										<option value="Canada">Canada</option>
										<option value="Canary Islands">Canary Islands</option>
										<option value="Cape Verde">Cape Verde</option>
										<option value="Cayman Islands">Cayman Islands</option>
										<option value="Central African Republic">Central African Republic</option>
										<option value="Chad">Chad</option>
										<option value="Channel Islands">Channel Islands</option>
										<option value="Chile">Chile</option>
										<option value="China">China</option>
										<option value="Christmas Island">Christmas Island</option>
										<option value="Cocos Island">Cocos Island</option>
										<option value="Colombia">Colombia</option>
										<option value="Comoros">Comoros</option>
										<option value="Congo">Congo</option>
										<option value="Cook Islands">Cook Islands</option>
										<option value="Costa Rica">Costa Rica</option>
										<option value="Cote DIvoire">Cote D'Ivoire</option>
										<option value="Croatia">Croatia</option>
										<option value="Cuba">Cuba</option>
										<option value="Curaco">Curacao</option>
										<option value="Cyprus">Cyprus</option>
										<option value="Czech Republic">Czech Republic</option>
										<option value="Denmark">Denmark</option>
										<option value="Djibouti">Djibouti</option>
										<option value="Dominica">Dominica</option>
										<option value="Dominican Republic">Dominican Republic</option>
										<option value="East Timor">East Timor</option>
										<option value="Ecuador">Ecuador</option>
										<option value="Egypt">Egypt</option>
										<option value="El Salvador">El Salvador</option>
										<option value="Equatorial Guinea">Equatorial Guinea</option>
										<option value="Eritrea">Eritrea</option>
										<option value="Estonia">Estonia</option>
										<option value="Ethiopia">Ethiopia</option>
										<option value="Falkland Islands">Falkland Islands</option>
										<option value="Faroe Islands">Faroe Islands</option>
										<option value="Fiji">Fiji</option>
										<option value="Finland">Finland</option>
										<option value="France">France</option>
										<option value="French Guiana">French Guiana</option>
										<option value="French Polynesia">French Polynesia</option>
										<option value="French Southern Ter">French Southern Ter</option>
										<option value="Gabon">Gabon</option>
										<option value="Gambia">Gambia</option>
										<option value="Georgia">Georgia</option>
										<option value="Germany">Germany</option>
										<option value="Ghana">Ghana</option>
										<option value="Gibraltar">Gibraltar</option>
										<option value="Great Britain">Great Britain</option>
										<option value="Greece">Greece</option>
										<option value="Greenland">Greenland</option>
										<option value="Grenada">Grenada</option>
										<option value="Guadeloupe">Guadeloupe</option>
										<option value="Guam">Guam</option>
										<option value="Guatemala">Guatemala</option>
										<option value="Guinea">Guinea</option>
										<option value="Guyana">Guyana</option>
										<option value="Haiti">Haiti</option>
										<option value="Hawaii">Hawaii</option>
										<option value="Honduras">Honduras</option>
										<option value="Hong Kong">Hong Kong</option>
										<option value="Hungary">Hungary</option>
										<option value="Iceland">Iceland</option>
										<option value="India">India</option>
										<option value="Indonesia">Indonesia</option>
										<option value="Iran">Iran</option>
										<option value="Iraq">Iraq</option>
										<option value="Ireland">Ireland</option>
										<option value="Isle of Man">Isle of Man</option>
										<option value="Israel">Israel</option>
										<option value="Italy">Italy</option>
										<option value="Jamaica">Jamaica</option>
										<option value="Japan">Japan</option>
										<option value="Jordan">Jordan</option>
										<option value="Kazakhstan">Kazakhstan</option>
										<option value="Kenya">Kenya</option>
										<option value="Kiribati">Kiribati</option>
										<option value="Korea North">Korea North</option>
										<option value="Korea Sout">Korea South</option>
										<option value="Kuwait">Kuwait</option>
										<option value="Kyrgyzstan">Kyrgyzstan</option>
										<option value="Laos">Laos</option>
										<option value="Latvia">Latvia</option>
										<option value="Lebanon">Lebanon</option>
										<option value="Lesotho">Lesotho</option>
										<option value="Liberia">Liberia</option>
										<option value="Libya">Libya</option>
										<option value="Liechtenstein">Liechtenstein</option>
										<option value="Lithuania">Lithuania</option>
										<option value="Luxembourg">Luxembourg</option>
										<option value="Macau">Macau</option>
										<option value="Macedonia">Macedonia</option>
										<option value="Madagascar">Madagascar</option>
										<option value="Malaysia">Malaysia</option>
										<option value="Malawi">Malawi</option>
										<option value="Maldives">Maldives</option>
										<option value="Mali">Mali</option>
										<option value="Malta">Malta</option>
										<option value="Marshall Islands">Marshall Islands</option>
										<option value="Martinique">Martinique</option>
										<option value="Mauritania">Mauritania</option>
										<option value="Mauritius">Mauritius</option>
										<option value="Mayotte">Mayotte</option>
										<option value="Mexico">Mexico</option>
										<option value="Midway Islands">Midway Islands</option>
										<option value="Moldova">Moldova</option>
										<option value="Monaco">Monaco</option>
										<option value="Mongolia">Mongolia</option>
										<option value="Montserrat">Montserrat</option>
										<option value="Morocco">Morocco</option>
										<option value="Mozambique">Mozambique</option>
										<option value="Myanmar">Myanmar</option>
										<option value="Nambia">Nambia</option>
										<option value="Nauru">Nauru</option>
										<option value="Nepal">Nepal</option>
										<option value="Netherland Antilles">Netherland Antilles</option>
										<option value="Netherlands">Netherlands (Holland, Europe)</option>
										<option value="Nevis">Nevis</option>
										<option value="New Caledonia">New Caledonia</option>
										<option value="New Zealand">New Zealand</option>
										<option value="Nicaragua">Nicaragua</option>
										<option value="Niger">Niger</option>
										<option value="Nigeria">Nigeria</option>
										<option value="Niue">Niue</option>
										<option value="Norfolk Island">Norfolk Island</option>
										<option value="Norway">Norway</option>
										<option value="Oman">Oman</option>
										<option value="Pakistan">Pakistan</option>
										<option value="Palau Island">Palau Island</option>
										<option value="Palestine">Palestine</option>
										<option value="Panama">Panama</option>
										<option value="Papua New Guinea">Papua New Guinea</option>
										<option value="Paraguay">Paraguay</option>
										<option value="Peru">Peru</option>
										<option value="Phillipines">Philippines</option>
										<option value="Pitcairn Island">Pitcairn Island</option>
										<option value="Poland">Poland</option>
										<option value="Portugal">Portugal</option>
										<option value="Puerto Rico">Puerto Rico</option>
										<option value="Qatar">Qatar</option>
										<option value="Republic of Montenegro">Republic of Montenegro</option>
										<option value="Republic of Serbia">Republic of Serbia</option>
										<option value="Reunion">Reunion</option>
										<option value="Romania">Romania</option>
										<option value="Russia">Russia</option>
										<option value="Rwanda">Rwanda</option>
										<option value="St Barthelemy">St Barthelemy</option>
										<option value="St Eustatius">St Eustatius</option>
										<option value="St Helena">St Helena</option>
										<option value="St Kitts-Nevis">St Kitts-Nevis</option>
										<option value="St Lucia">St Lucia</option>
										<option value="St Maarten">St Maarten</option>
										<option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
										<option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
										<option value="Saipan">Saipan</option>
										<option value="Samoa">Samoa</option>
										<option value="Samoa American">Samoa American</option>
										<option value="San Marino">San Marino</option>
										<option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
										<option value="Saudi Arabia">Saudi Arabia</option>
										<option value="Senegal">Senegal</option>
										<option value="Serbia">Serbia</option>
										<option value="Seychelles">Seychelles</option>
										<option value="Sierra Leone">Sierra Leone</option>
										<option value="Singapore">Singapore</option>
										<option value="Slovakia">Slovakia</option>
										<option value="Slovenia">Slovenia</option>
										<option value="Solomon Islands">Solomon Islands</option>
										<option value="Somalia">Somalia</option>
										<option value="South Africa">South Africa</option>
										<option value="Spain">Spain</option>
										<option value="Sri Lanka">Sri Lanka</option>
										<option value="Sudan">Sudan</option>
										<option value="Suriname">Suriname</option>
										<option value="Swaziland">Swaziland</option>
										<option value="Sweden">Sweden</option>
										<option value="Switzerland">Switzerland</option>
										<option value="Syria">Syria</option>
										<option value="Tahiti">Tahiti</option>
										<option value="Taiwan">Taiwan</option>
										<option value="Tajikistan">Tajikistan</option>
										<option value="Tanzania">Tanzania</option>
										<option value="Thailand">Thailand</option>
										<option value="Togo">Togo</option>
										<option value="Tokelau">Tokelau</option>
										<option value="Tonga">Tonga</option>
										<option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
										<option value="Tunisia">Tunisia</option>
										<option value="Turkey">Turkey</option>
										<option value="Turkmenistan">Turkmenistan</option>
										<option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
										<option value="Tuvalu">Tuvalu</option>
										<option value="Uganda">Uganda</option>
										<option value="Ukraine">Ukraine</option>
										<option value="United Arab Erimates">United Arab Emirates</option>
										<option value="United Kingdom">United Kingdom</option>
										<option value="United States of America">United States of America</option>
										<option value="Uraguay">Uruguay</option>
										<option value="Uzbekistan">Uzbekistan</option>
										<option value="Vanuatu">Vanuatu</option>
										<option value="Vatican City State">Vatican City State</option>
										<option value="Venezuela">Venezuela</option>
										<option value="Vietnam">Vietnam</option>
										<option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
										<option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
										<option value="Wake Island">Wake Island</option>
										<option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
										<option value="Yemen">Yemen</option>
										<option value="Zaire">Zaire</option>
										<option value="Zambia">Zambia</option>
										<option value="Zimbabwe">Zimbabwe</option>
								</select>
								<div className="input text-red"></div>
							</div>
						</div>
					</div>

					<div className="form-group">
						<div>
							<div className='form-group'>
								City
								<input type="text"
		                            name="city"
		                            className="form-control"
		                            placeholder="City"
		                            ref="city"/>
						        
							</div>
						</div>
					</div>
					<div className="form-group">
						<div>
						Facebook Link
							<div className='form-group'>
								<input type="text"
										name="facebook"
										className="form-control"
										placeholder="Facebook Link"
										ref="facebook"/>
								<div className="input text-red"></div>
							</div>
						</div>
					</div>
					Linkedin Link
					<div className="form-group">
						<div>
							<div className='form-group'>
								<input type="text"
										name="linkedin"
										className="form-control"
										placeholder="Linkedin Link"
										ref="linkedin"/>
								<div className="input text-red"></div>
							</div>
						</div>
					</div>
					Git Link
					<div className="form-group">
						<div>
							<div className='form-group'>
								<input type="text"
										name="git"
										className="form-control"
										placeholder="Git Link"
										ref="git"/>
								<div className="input text-red"></div>
							</div>
						</div>
					</div>
					<div className="col-md-6 col-md-offset-5">
		      			<button  className="btn btn-success" onClick={this.create}>Get Started</button>
		      		</div>
      			</div>

      		</div>
      		
      		<br /><br /><br />
      	</div>
      	</div>
      	: 
      	<div/>}



      		<Modal show={this.state.showmodal}>
                <Modal.Body>
                  We will review your profile.  You will be able to update it by adding more informations about your self.
                </Modal.Body>
                <Modal.Footer>
                  <button  className="btn btn-success" onClick={this.YesConfirmation}>I understand</button>
                </Modal.Footer>
              </Modal>


      </div>
    );
  }

}