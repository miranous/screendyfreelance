import React, { Component } from 'react';
import linkState from 'react-link-state';
import DevListSearch from '../developpers/DeveloppersListSearch';
import DevelopperActions from '../actions/DevelopperActions';

export default class Search extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
        SearchValue: this.props.params.keyword,
        loading: true
      }
  }

  keydown(){
    this.refs.text.style.color="black";
    this.setState({SearchValue : this.state.SearchValue});
  }
  Search(event){
    if(this.state.SearchValue){
      this.setState({loading : true});
      this.props.history.pushState(null, `Search/1/${this.state.SearchValue}`);
      DevelopperActions.GetDeveloppersSearch(this.state.SearchValue);
    }
    else
    {
      this.refs.text.style.color="red";
      this.refs.text.value="Missing keyword...";
    }
  }

  SearchEnter(event){
    if (event.key === 'Enter') {
      if(this.state.SearchValue){
        this.setState({loading : true});
        this.props.history.pushState(null, `Search/1/${this.state.SearchValue}`);
        DevelopperActions.GetDeveloppersSearch(this.state.SearchValue);
      }
      else
      {
        this.refs.text.style.color="red";
        this.refs.text.value="Missing keyword...";
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
     SearchValue : this.props.params.keyword
    });
  }

  componentWillUpdate () {
    window.scrollTo(0, 0)
  }

  render() {
  	var Search = this.Search.bind(this);
    var keydown = this.keydown.bind(this);
    var SearchEnter = this.SearchEnter.bind(this);
    return (
     <div className ="row">
      <div className="container">
          <h2 className="text-center">Search Results</h2>
          <br/>
          <div className="col-md-2"></div>
                <div className="col-md-8">
                   <div className="input-group">
                             <input onKeyPress={SearchEnter} type="text" name="search" className="form-control" ref="text" placeholder="Search..." onClick={keydown} valueLink={linkState(this, 'SearchValue')}/>
                             <span className="input-group-btn">
                               <button type="button" name="search" id="search-btn" className="btn btn-flat" onClick={Search}><i className="fa fa-search"></i></button>
                            </span>
                    </div>
             
                </div>
         
          <br/><br/><br/><br/><br/>
        
       

        <DevListSearch loading={this.state.loading} page={this.props.params.page} changeRoute={this.props.history.pushState} search={this.props.params.keyword}/>

        </div>
      </div>
    );
  }

}

