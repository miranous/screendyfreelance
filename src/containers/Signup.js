import React, { Component } from 'react';
import { Link } from 'react-router';
import SignupStore from '../stores/SignupStore';
import UserActions from '../actions/UserActions';

class Signup extends Component {


  constructor(props, context) {

    super(props, context);
    this.state={
      isSigninUp : false,
      fine : true,
      response : null,
      message : ''
    }
    this._updateAuth=this._updateAuth.bind(this);
    this.handleSubmit=this.handleSubmit.bind(this);
    this.Response=this.Response.bind(this);
  }

 
  componentWillUpdate () {
    window.scrollTo(0, 0)
  }
  _updateAuth(){

    this.setState({
      isSigninUp : false,
      fine : SignupStore.isSignedUp().FINE,
      response : SignupStore.isSignedUp().response,
      message : SignupStore.isSignedUp().message
    });
  }
  componentWillMount() {
    SignupStore.addChangeListener(this._updateAuth);
  }

  componentWillUnmount() {
    SignupStore.removeChangeListener(this._updateAuth);
  }


  handleSubmit(event) {
    event.preventDefault();

    const email    = this.refs.email.value;
    const password = this.refs.password.value;
    const passwordConfirmation = this.refs.passwordConfirmation.value;
    const firstName = this.refs.firstName.value;
    const lastName = this.refs.lastName.value;

    this.setState({ isSigninUp: true });

    UserActions.signUpTheUser({email,password,passwordConfirmation,firstName,lastName});
  
  }

  render() {
    if(this.state.response != null)
    return this.Response();
    else
    {
    if(this.state.isSigninUp)
    return this._renderHTMLLoading();
    return this._renderHTML();
    }
  }
  Response(){
      return(
    <div className="register-box">
    <div className="register-logo">
      <img src="http://web.screendy.com/wp-content/uploads/2015/05/logoScreendy.png" height="50"/>
    </div>
    <div className="register-box-body">
    <p className="login-box-msg">{this.state.message}</p>
    
    <div className="row">
    <div className="col-xs-8">
    {this.state.response ?
    <Link to='/signin' className="text-center">Signin</Link>
    :
    <a href="http://screendydev.cloudapp.net/screendy/front/studio/v097917/?msg=#" target="_blank">I forgot my password</a>
    }
    </div>
    </div>
    </div>
    </div>
    );
    
  }
  _renderHTML() {
    return (
  <div className="register-box">
  <div className="register-logo">
    <img src="http://web.screendy.com/wp-content/uploads/2015/05/logoScreendy.png" height="50"/>
  </div>
  {this.state.fine ? 
  <div className="register-box-body">
            <p className="login-box-msg">Register a new membership</p>
    <form onSubmit={this.handleSubmit}>
      <div className="form-group has-feedback">
        <input type="text" ref="firstName" className="form-control" placeholder="First name"/>
        <span className="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="text" ref="lastName" className="form-control" placeholder="Last name"/>
        <span className="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="email" ref="email" className="form-control" placeholder="Email"/>
        <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="password" ref="password" className="form-control" placeholder="Password"/>
        <span className="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="password" ref="passwordConfirmation" className="form-control" placeholder="Retype password"/>
        <span className="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div className="row">
        <div className="col-xs-8">
            <Link to='/signin' className="text-center">I already have a membership</Link>
        </div>
        <div className="col-xs-4">
          <button type="submit" className="btn btn-primary btn-block btn-flat">Register</button>
        </div>
      </div>
    </form>
    
  </div>
  :
  <div className="register-box-body">
    <p className="login-box-msg">A problem occured, please try again in a few seconds</p>
    </div>
  }
</div>
    );
  }

  _renderHTMLLoading() {
    return(
      <div className="register-box">
  <div className="register-logo">
    <img src="http://web.screendy.com/wp-content/uploads/2015/05/logoScreendy.png" height="50"/>
  </div>

  <div className="box register-box-body">
            <p className="login-box-msg">Processing signup</p>
    <form onSubmit={this.handleSubmit}>
      <div className="form-group has-feedback">
        <input type="text" ref="firstName" className="form-control" placeholder="First name"/>
        <span className="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="text" ref="lastName" className="form-control" placeholder="Last name"/>
        <span className="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="email" ref="email" className="form-control" placeholder="Email"/>
        <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="password" ref="password" className="form-control" placeholder="Password"/>
        <span className="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div className="form-group has-feedback">
        <input type="password" ref="passwordConfirmation" className="form-control" placeholder="Retype password"/>
        <span className="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div className="row">
        <div className="col-xs-8">
            <Link to='/signin' className="text-center">I already have a membership</Link>
        </div>
        <div className="col-xs-4">
          <button type="submit" className="btn btn-primary btn-block btn-flat">Register</button>
        </div>
      </div>
    </form>


    <div className="overlay">
          <i className="fa fa-refresh fa-spin"></i>
    </div>
    
  </div>
</div>
    
      );
  }

}

Signup.contextTypes = {
  history: React.PropTypes.object
};

export default Signup
