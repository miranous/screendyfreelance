import React from 'react';
import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';


// CONTAINERS
import Header from './Header';
import Sidebar from './Sidebar';
import Aside from './Aside';
import ContentContainer from './ContentContainer';
import Footer from './Footer';
import SecondHeader from './SecondHeader';
import Signin from './Signin';



export default class Container extends React.Component {

  constructor(props, context) {
    super(props, context);
    UserActions.is_active();
  }


	componentDidMount() {
    
  }

  componentWillUnmount() {
    
  }

  render() {
  

  	return (
			<div className="content-wrapper">
      	<Header changeRoute={this.props.history.pushState} />
        <SecondHeader />
      	<ContentContainer children={this.props.children} />
      	<Footer />
    	</div>
		);
  }

}

